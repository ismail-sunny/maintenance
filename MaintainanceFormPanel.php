<?php
include 'header.php';
?>
<section id="content">
    <div class="container">
        <div class="row">
            <!--ul class="tabs tab-demo-active z-depth-1">
                <li class="tab col s3"><a class="white-text red darken-1 waves-effect waves-light active" href="#maintenances"><i class="mdi-action-perm-identity"></i> Maintenance Forms</a>
                </li>
                <li class="tab col s3"><a class="white-text purple darken-1 waves-effect waves-light" href="#templates"><i class="mdi-action-settings-display"></i> Form Templates</a>
                </li>
            </ul-->
            <div id="maintenances" class="col s12">
                <div class="row">
                    <!--div class="col s12 m8 l9 card-panel" ng-show="formDetailShow">
                        <h5 class="header">Form Detail</h5>
                        <b class="header">{{formDetail.name}}</b><br>
                        <b class="header">{{formDetail.description}}</b><br>
                        <b class="header">Category: {{formDetail.category}}</b>
                        <ul class="collection with-header">
                            <li class="collection-header">
                                <b>Basic Informations</b>
                            </li>
                            <li class="collection-item" ng-repeat="customInfo in formDetail.formCustomInfos">
                                <blockquote><b>{{$index+1}} {{customInfo.description}}</b></blockquote>
                            </li>
                        </ul>

                        <ul class="collection with-header">
                            <li class="collection-header">
                                <b>Check Lists</b>
                            </li>
                            <li class="collection-item" ng-repeat="formField in formDetail.formFields">
                                <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                                <p>Type : {{formField.type}}
                                    <br> Values: {{formField.selection_values}}
                                    <br> Positive Value: {{formField.positive_value}}
                                    <br> Negative Value: {{formField.negative_value}}
                                </p>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="input-field" style="right: 10px;">
                                <button class="btn red waves-effect waves-light right" ng-click="CloseFormDetail()">Close
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div>
                        </div>
                    </div-->
                    <!--div ng-show="formView">
                        <form class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                    <h4>Create Maintainance Form</h4><br>
                                    <b>{{formData.name}}<br>
                                        {{formData.description}}<br>
                                        Category: {{formData.category}}</b>
                                </div>
                            </div>
                            <ul class="collection with-header yellow darken-4">
                                <li class="collection-header">
                                    <h6><b>Form Custom Information</b></h6>
                                </li>
                                <li class="collection-item" ng-repeat="formField in formData.formCustomInfos">
                                    <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="" ng-model="formField.value" type="text">
                                            <label>{{formField.description}}</label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="collection with-header green accent-4">
                                <li class="collection-header">
                                    <b>Check Lists</b>
                                </li>
                                <li class="collection-item" ng-repeat="formField in formData.formFields">
                                    <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                                    <p>Type : {{formField.type}}
                                        <br> Values: {{formField.selection_values}}
                                        <br> Positive Value: {{formField.positive_value}}
                                        <br> Negative Value: {{formField.negative_value}}
                                    </p>
                                </li>
                            </ul>


                            <div class="row">
                                <div class="input-field" style="right: 10px;">
                                    <button class="btn red waves-effect waves-light right" ng-click="CloseForm()">Cancel
                                        <i class="mdi-navigation-cancel right"></i>
                                    </button>
                                </div>
                                <div class="input-field" style="right: 20px;">
                                    <button class="btn cyan waves-effect waves-light right" ng-click="SubmitMaintenanceForm()">Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div-->
                    <div ng-show="mainDataListView">
                        <h4 class="header">Maintenance Sheet List</h4>
                        <!--table class="responsive-table display stripped" cellspacing="0"-->
                        <table class="stripped bordered">
                            <thead>
                                <tr class="customth">
                                    <th>Name/Description</th>
                                    <th>Category</th>
                                    <th>Infos</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="x in maintanenceFormList">
                                    <td>{{ x.name }}<br>{{ x.description }}</td>
                                    <td>{{ x.category}}</td>
                                    <td>
                                        <p ng-repeat="info in x.formCustomInfos"><b>{{info.description}}:</b>{{info.value}}</p>
                                    </td>
                                    <td>
                                        <!--button class="btn cyan waves-effect waves-light" style="right: 10px;" ng-click="GetMaintenanceSheetList(x.maintenance_id,x.form_id)"><i class="mdi-content-content-paste"></i>Schedule/Data</button-->
                                        <div class="tooltip">
                                            <a class="btn-floating waves-effect waves-light light-green" ng-click="GetMaintenanceSheetList(x.maintenance_id,x.form_id)"><i class="mdi-content-content-paste"></i></a>
                                            <span class="tooltiptext">Schedule/Data</span>
                                        </div>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div ng-show="scheduleListView">
                        <div class="card-panel">
                            <blockquote><b>Maintenance Schedule/Data List</b> <a class="btn-floating waves-effect waves-light" ng-click="CloseScheduleList()"><i class="mdi-content-clear"></i></a>
                                <!--a class="btn-floating waves-effect waves-light" ng-click="PDFReport()"><i class="mdi-content-clear"></i></a-->
                            </blockquote>
                            <b>Name: <i>{{maintenanceSheetList[0].name}}</i></b><br>
                            <b>Description: <i>{{maintenanceSheetList[0].description}}</i></b><br>
                            <b>Category: <i>{{maintenanceSheetList[0].category}}</i></b><br>
                            <b ng-repeat="x in maintenanceSheetList[0].formCustomInfos">{{x.description}} :<i>{{x.value}}</i><br></b>
                            <!--table class="responsive-table display stripped" cellspacing="0"-->
                        </div>
                        <table class="stripped bordered" id="mainTableData" get-html="$ctrl.string = $content">
                            <thead>
                                <tr class="customth">
                                    <th>Schedule On</th>
                                    <th>Status</th>
                                    <th>Completed On</th>
                                    <th ng-repeat="x in maintenanceSheetList[0].formFields">{{x.description}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="sheet in maintenanceSheetList">
                                    <td>{{sheet.schedule_on}}</td>
                                    <td>{{sheet.status}}</td>
                                    <td>{{sheet.completed_on}}</td>
                                    <td ng-repeat="x in sheet.formFields">{{ x.value }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <!--{{ $ctrl.string }}-->
                    </div>

                    <div ng-show="scheduleFormView">
                        <form class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                    <h4>Create Maintenance Schedule</h4><br>
                                    <b>Name: <i>{{scheduleForm.name}}</i></b><br>
                                    <b>Description: <i>{{scheduleForm.description}}</i></b><br>
                                    <b>Category: <i>{{scheduleForm.category}}</i></b><br>
                                    <b ng-repeat="x in scheduleForm.formCustomInfos">{{x.description}} :<i>{{x.value}}</i><br></b>
                                </div>
                            </div>
                            <ul class="collection with-header">
                                <li class="collection-header">
                                    <h6><b>Schedule Information</b></h6>
                                </li>
                                <li class="collection-item">
                                    <label>Assign To</label><br>
                                    <select ng-model="scheduleForm.assign_to">
                                        <option value="" disabled selected>Select User</option>
                                        <option value="1">User 1</option>
                                        <option value="2">User 2</option>
                                        <option value="3">User 3</option>
                                    </select>
                                </li>
                                <li class="collection-item">
                                    <label>Status</label><br>
                                    <select ng-model="scheduleForm.status">
                                        <option value="" disabled selected>Choose your option</option>
                                        <option value="New">New</option>
                                        <option value="New2">Option 2</option>
                                        <option value="New3">Option 3</option>
                                    </select>
                                </li>
                                <li class="collection-item">
                                    <label>Remarks</label><br>
                                    <input ng-model="scheduleForm.remarks" placeholder="Remarks" type="text">
                                </li>
                                <li class="collection-item">
                                    <label>Schedule On</label><br>
                                    <input ng-model="scheduleForm.schedule_on" type="date" class="datepicker">
                                </li>
                            </ul>
                            <div class="row">
                                <div class="input-field" style="right: 10px;">
                                    <button class="btn red waves-effect waves-light right" ng-click="CloseScheduleForm()">Cancel
                                        <i class="mdi-navigation-cancel right"></i>
                                    </button>
                                </div>
                                <div class="input-field" style="right: 20px;">
                                    <button class="btn cyan waves-effect waves-light right" ng-click="SubmitMaintenanceScheduleForm(scheduleForm)">Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!--div id="templates" class="col s12">
                <div class="row card-panel" ng-show="formDetailShow">
                    <h5 class="header">Form Detail</h5>
                    <b class="header">{{formDetail.name}}</b><br>
                    <b class="header">{{formDetail.description}}</b><br>
                    <b class="header">Category: {{formDetail.category}}</b>
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <b>Basic Informations</b>
                        </li>
                        <li class="collection-item" ng-repeat="customInfo in formDetail.formCustomInfos">
                            <blockquote><b>{{$index+1}} {{customInfo.description}}</b></blockquote>
                        </li>
                    </ul>

                    <ul class="collection with-header">
                        <li class="collection-header">
                            <b>Check Lists</b>
                        </li>
                        <li class="collection-item" ng-repeat="formField in formDetail.formFields">
                            <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                            <p>Type : {{formField.type}}
                                <br> Values: {{formField.selection_values}}
                                <br> Positive Value: {{formField.positive_value}}
                                <br> Negative Value: {{formField.negative_value}}
                            </p>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="input-field" style="right: 10px;">
                            <button class="btn red waves-effect waves-light right" ng-click="CloseFormDetail()">Close
                                <i class="mdi-navigation-cancel right"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div ng-show="formView">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <h4>Create Maintainance Form</h4><br>
                                <b>{{formData.name}}<br>
                                    {{formData.description}}<br>
                                    Category: {{formData.category}}</b>
                            </div>
                        </div>
                        <ul class="collection with-header yellow darken-4">
                            <li class="collection-header">
                                <h6><b>Form Custom Information</b></h6>
                            </li>
                            <li class="collection-item" ng-repeat="formField in formData.formCustomInfos">
                                <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="formField.value" type="text">
                                        <label>{{formField.description}}</label>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <ul class="collection with-header green accent-4">
                            <li class="collection-header">
                                <b>Check Lists</b>
                            </li>
                            <li class="collection-item" ng-repeat="formField in formData.formFields">
                                <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                                <p>Type : {{formField.type}}
                                    <br> Values: {{formField.selection_values}}
                                    <br> Positive Value: {{formField.positive_value}}
                                    <br> Negative Value: {{formField.negative_value}}
                                </p>
                            </li>
                        </ul>


                        <div class="row">
                            <div class="input-field" style="right: 10px;">
                                <button class="btn red waves-effect waves-light right" ng-click="CloseForm()">Cancel
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div>
                            <div class="input-field" style="right: 20px;">
                                <button class="btn cyan waves-effect waves-light right" ng-click="SubmitMaintenanceForm()">Submit
                                    <i class="mdi-content-send right"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div ng-show="dataListView">
                    <h4 class="header">Form List</h4>
                    <table class="stripped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="x in formList">
                                <td>{{ x.name }}</td>
                                <td>{{ x.description }}</td>
                                <td>{{ x.category}}</td>
                                <td>
                                    <div class="tooltip">
                                        <a class="btn-floating waves-effect waves-light deep-orange" ng-click="CreateMaintenanceSheetForm(x.id)"><i class="mdi-action-assignment-turned-in"></i></a>
                                        <span class="tooltiptext">New Maintainace Sheet</span>
                                    </div>
                                    <div class="tooltip">
                                        <a class="btn-floating waves-effect waves-light blue-grey" ng-click="GetFormDetail(x.id)"><i class="mdi-action-assignment"></i></a>
                                        <span class="tooltiptext">Detail</span>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div-->
        </div>
        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;" ng-show="floatShow">
            <a class="btn-floating btn-large" ng-click="ShowScheduleForm(maintenanceSheetList[0])">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        <!-- Floating Action Button -->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<?php
include 'footer.php';
?>

<script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>



<script>
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 5, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd'
    });
    var myApp = angular.module('MaintenanceApp', ['ui.materialize', 'oitozero.ngSweetAlert']);


    //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
    myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location, $window, $timeout, SweetAlert) {
        $scope.formView = false;
        $scope.floatShow = false;
        $scope.dataListView = true;
        $scope.mainDataListView = true;

        $scope.demo1 = function() {
            SweetAlert.swal("Here's a message");
        }
        $scope.demo2 = function() {
            SweetAlert.swal("Here's a message!", "It's pretty, isn't it?");
        }

        $scope.demo3 = function() {
            SweetAlert.swal("Good job!", "You clicked the button!", "success")
        }
        $scope.demo4 = function() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function() {
                SweetAlert.swal("Booyah!");
            });
        }
        $scope.demo5 = function() {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Your will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel plx!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    SweetAlert.swal("Deleted!", "Your imaginary file has been deleted.", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Your imaginary file is safe :)", "error");
                }
            });
        }



        $scope.demo6 = function() {
            SweetAlert.swal({
                title: "Sweet!",
                text: "Here's a custom image.",
                imageUrl: "https://avatars0.githubusercontent.com/u/4194490?s=400&v=4"
            });
        }


        $scope.absUrl = $location.absUrl();
        //console.log($scope.absUrl);

        $scope.LogOut = function() {
            $http.post('apis/logout.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                $scope.errorMessage = response.data.Message;
                $window.location.href = '/Login.php';
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };

        $scope.formFieldList = [{
            description: '',
            values: '',
            type: '',
            positive_value: '',
            negative_value: ''
        }];

        $scope.formCustomInfoList = [{
            description: '',
            info_type: 'custom'
        }];

        $scope.formFieldListInDb = $scope.formFieldList;

        $scope.ResetForm = function() {
            $scope.formData = {
                name: "",
                description: "",
                id: 0
            };
        }

        $scope.ResetForm();

        $scope.ShowForm = function() {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.formDetailShow = false;
        };

        $scope.ShowScheduleForm = function(value) {
            $scope.scheduleForm = value;
            $scope.scheduleFormView = true;
            $scope.scheduleListView = false;
            $scope.mainDataListView = false;
            $scope.floatShow = false;
        };


        $scope.CloseScheduleForm = function() {
            $scope.scheduleFormView = false;
            $scope.scheduleListView = true;
            $scope.mainDataListView = false;
            $scope.floatShow = true;
        };


        $scope.CloseForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
            $scope.ResetForm();
        };

        $scope.CloseFormDetail = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
        };

        $scope.CloseScheduleList = function() {
            $scope.mainDataListView = true;
            $scope.scheduleListView = false;
            $scope.floatShow = false;
        };

        $scope.getFormList = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetFormList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.formList = response.data.Response;
                console.log($scope.formList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getFormList();

        $scope.getCategory = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetCategoryList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.categoryList = response.data.Response;
                console.log($scope.categoryList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getCategory();

        $scope.getMaintenanceFormList = function() {
            $scope.loadingR = true;
            SweetAlert.swal({
                title: "Loading...",
                imageUrl: "images/loading2.gif",
                showCancelButton: false,
                showConfirmButton: false,
                html: true,
            });
            $http({
                method: 'GET',
                url: 'apis/GetMaintenanceFormList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.maintanenceFormList = response.data.Response;
                SweetAlert.swal.close();
                console.log($scope.maintanenceFormList);
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getMaintenanceFormList();

        $scope.GetFormDetail = function(value) {
            var data = {
                form_id: value
            };
            $http.post('apis/GetFormDetail.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data.Response);
                $scope.formDetail = response.data.Response;
                $scope.formView = false;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.formDetailShow = true;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };

        $scope.PDFReport = function() {
            var data = angular.element("#mainTableData")[0].outerHTML;
            //console.log(data);
            /*$http.post('MaintenanceReportPdf.php?', data).then(function(response) {
                console.log(response.data);
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });*/
        };

        $scope.CreateMaintenanceSheetForm = function(value) {
            var data = {
                form_id: value
            };
            $http.post('apis/GetFormDetail.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data.Response);
                $scope.formData = response.data.Response;
                $scope.formView = true;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.formDetailShow = false;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };



        $scope.GetMaintenanceSheetList = function(maintanenceId, formId) {
            var data = {
                maintenance_id: maintanenceId,
                form_id: formId
            };
            /*SweetAlert.swal("Loading", {
                button: false,
            });*/
            //SweetAlert.swal("Here's a message!", "It's pretty, isn't it?");

            SweetAlert.swal({
                title: "Loading...",
                imageUrl: "images/loading2.gif",
                showCancelButton: false,
                showConfirmButton: false,
                html: true,
            });
            $http.post('apis/GetMaintenanceSheetList.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data);
                $scope.maintenanceSheetList = response.data.Response;
                //$scope.formView = true;
                //$scope.dataListView = false;
                //$scope.floatShow = false;
                //$scope.formDetailShow = false;
                $scope.scheduleListView = true;
                $scope.mainDataListView = false;
                $scope.floatShow = true;
                SweetAlert.swal.close();
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };

        $scope.SubmitMaintenanceScheduleForm = function(value) {
            $scope.scheduleFormView = false;
            $scope.scheduleListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            $scope.formData = value;
            $scope.formData.tag = "add";

            $http.post('apis/CreateMaintenanceSchedule.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                if (response.data === 'success') {
                    $scope.CloseFormDetail();
                    $scope.GetMaintenanceSheetList($scope.formData.maintenance_id, $scope.formData.form_id);
                    SweetAlert.swal("Successfull", "Successfully Added!", "success");
                    //$scope.getFormList();
                } else {
                    SweetAlert.swal("Error", response.data);
                }
            }, function(response) {
                $scope.msg = "Service not Exists";
                SweetAlert.swal("Error", "Service not Exists");
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };


        $scope.SubmitMaintenanceForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            console.log($scope.selectedCategory);
            console.log($scope.formData);
            $scope.formData.tag = "add";

            $http.post('apis/CreateMaintenanceForm.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                if (response.data === 'success') {
                    $scope.ResetForm();
                    $scope.CloseForm();
                    //$scope.getFormList();
                }
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };



        $scope.$watch('op', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });
        $scope.$watch('type', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });

    });

    myApp.directive('getHtml', function() {
        return {
            scope: {
                getHtml: '&'
            },
            restrict: 'A',
            link: function(scope, elem, attr) {
                scope.getHtml({
                    $content: elem.html()
                });
            }
        };
    });
</script>
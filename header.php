<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="description" content="Lets mantain everything">
  <meta name="keywords" content="mantenance">
  <title>Maintenance - CEAS</title>

  <!-- Favicons-->
  <link rel="icon" href="images/maintenance.png" sizes="32x32">
  <!-- Favicons-->
  <link rel="apple-touch-icon-precomposed" href="images/maintenance.png">
  <!-- For iPhone -->
  <meta name="msapplication-TileColor" content="#00bcd4">
  <meta name="msapplication-TileImage" content="images/maintenance.png">
  <!-- For Windows Phone -->


  <!-- CORE CSS-->
  <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <!-- Custome CSS-->
  <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">


  <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
  <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="js/plugins/jvectormap/jquery-jvectormap.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="js/plugins/sweetalert/sweetalert.css" type="text/css" rel="stylesheet" media="screen,projection">
  <link href="js/plugins/data-tables/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet" media="screen,projection">
  <style>
    .tooltip {
      position: relative;
      display: inline-block;
    }

    .tooltip .tooltiptext {
      visibility: hidden;
      width: 120px;
      background-color: black;
      color: #fff;
      text-align: center;
      border-radius: 6px;
      padding: 5px 0;
      position: absolute;
      z-index: 1;
      bottom: 150%;
      left: 50%;
      margin-left: -60px;
    }

    .tooltip .tooltiptext::after {
      content: "";
      position: absolute;
      top: 100%;
      left: 50%;
      margin-left: -5px;
      border-width: 5px;
      border-style: solid;
      border-color: black transparent transparent transparent;
    }

    .tooltip:hover .tooltiptext {
      visibility: visible;
    }

    .customth {
      padding-top: 12px;
      padding-bottom: 12px;
      text-align: left;
      background-color: #4CAF50;
      color: white;
    }
  </style>
</head>

<?php
session_start();
if (!isset($_SESSION["email"])) {
  header("location: login.php");
}
//print_r($_SESSION);
?>

<body ng-app="MaintenanceApp" ng-controller="panelCtrl as $ctrl">
  <!-- Start Page Loading -->
  <div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
  </div>
  <!-- End Page Loading -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START HEADER -->
  <header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
      <nav class="navbar-color">
        <div class="nav-wrapper">
          <ul class="left">
            <li>
              <h1 class="logo-wrapper"><a href="index.html" class="brand-logo" style="padding:0;"><img height="55" src="images/maintenance-logo.png" alt="Maintenance Logo"></a> <span class="logo-text">Materialize</span></h1>
            </li>
          </ul>
          <!--div class="header-search-wrapper hide-on-med-and-down">
            <i class="mdi-action-search"></i>
            <input type="text" name="Search" class="header-search-input z-depth-2" placeholder="Explore Materialize" />
          </div-->
          <ul class="right hide-on-med-and-down">
            <!--li><a href="javascript:void(0);" class="waves-effect waves-block waves-light translation-button" data-activates="translation-dropdown"><img src="images/flag-icons/United-States.png" alt="USA" /></a>
            </li-->
            <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
            </li>
            <li><a class="waves-effect waves-block waves-light" ng-click="LogOut()"><i class="mdi-action-exit-to-app"></i></a>
            </li>
            <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light notification-button" data-activates="notifications-dropdown"><i class="mdi-social-notifications"><small class="notification-badge">5</small></i>
              </a>
            </li>
            <!--li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
            </li-->
          </ul>

          <!-- notifications-dropdown -->
          <ul id="notifications-dropdown" class="dropdown-content">
            <li>
              <h5>NOTIFICATIONS <span class="new badge">5</span></h5>
            </li>
            <li class="divider"></li>
            <li>
              <a href="#!"><i class="mdi-action-add-shopping-cart"></i> A new order has been placed!</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">2 hours ago</time>
            </li>
            <li>
              <a href="#!"><i class="mdi-action-stars"></i> Completed the task</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">3 days ago</time>
            </li>
            <li>
              <a href="#!"><i class="mdi-action-settings"></i> Settings updated</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">4 days ago</time>
            </li>
            <li>
              <a href="#!"><i class="mdi-editor-insert-invitation"></i> Director meeting started</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">6 days ago</time>
            </li>
            <li>
              <a href="#!"><i class="mdi-action-trending-up"></i> Generate monthly report</a>
              <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">1 week ago</time>
            </li>
          </ul>
        </div>
      </nav>
    </div>
    <!-- end header nav-->
  </header>
  <!-- END HEADER -->

  <!-- //////////////////////////////////////////////////////////////////////////// -->

  <!-- START MAIN -->
  <div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

      <!-- START LEFT SIDEBAR NAV-->
      <aside id="left-sidebar-nav">
        <ul id="slide-out" class="side-nav fixed leftside-navigation">
          <li class="user-details cyan darken-2">
            <div class="row">
              <div class="col col s4 m4 l4">
                <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
              </div>
              <div class="col col s8 m8 l8">
                <ul id="profile-dropdown" class="dropdown-content">
                  <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                  </li>
                  <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                  </li>
                  <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                  </li>
                  <li class="divider"></li>
                  <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                  </li>
                  <li><a href="#"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                  </li>
                </ul>
                <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown">John Doe<i class="mdi-navigation-arrow-drop-down right"></i></a>
                <p class="user-roal">Administrator</p>
              </div>
            </div>
          </li>
          <!--li class="bold active"><a href="index.html" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
          </li-->
          <li ng-class="absUrl.includes('SchedulePanel') ? 'active bold': 'bold'"><a href="SchedulePanel.php" class="waves-effect waves-cyan"><i class="mdi-action-view-list"></i> Schedule Sheet/Data</a>
          </li>
          <li ng-class="absUrl.includes('SchedulerPanel') ? 'active bold': 'bold'"><a href="SchedulerPanel.php" class="waves-effect waves-cyan"><i class="mdi-action-view-list"></i> Scheduler</a>
          </li>
          <li ng-class="absUrl.includes('MaintainanceFormPanel') ? 'active bold': 'bold'"><a href="MaintainanceFormPanel.php" class="waves-effect waves-cyan"><i class="mdi-av-my-library-books"></i> Maintenance Forms</a>
          </li>
          <li ng-class="absUrl.includes('FormTemplatePanel') ? 'active bold': 'bold'"><a href="FormTemplatePanel.php" class="waves-effect waves-cyan"><i class="mdi-av-my-library-books"></i> Form Template</a>
          </li>
          <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
              <li ng-class="(absUrl.includes('CategoryPanel') || absUrl.includes('FormBuilderPanel') || absUrl.includes('UserPanel'))? 'active bold': 'bold'"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-action-view-carousel"></i> Settings</a>
                <div class="collapsible-body">
                  <ul>
                    <li ng-class="absUrl.includes('CategoryPanel') ? 'active bold': 'bold'"><a href="CategoryPanel.php" class="waves-effect waves-cyan"><i class="mdi-av-playlist-add"></i> Category</a>
                    </li>
                    <li ng-class="absUrl.includes('FormBuilderPanel') ? 'active bold': 'bold'"><a href="FormBuilderPanel.php" class="waves-effect waves-cyan"><i class="mdi-action-description"></i> Form Builder</a>
                    </li>
                    <li ng-class="absUrl.includes('UserPanel') ? 'active bold': 'bold'"><a href="UserPanel.php" class="waves-effect waves-cyan"><i class="mdi-social-people"></i> Users</a>
                    </li>
                  </ul>
                </div>
              </li>
            </ul>
          </li>
        </ul>
        <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
      </aside>
      <!-- END LEFT SIDEBAR NAV-->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
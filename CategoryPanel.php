<?php
include 'header.php';
?>
<section id="content">
    <div class="container">

        <div id="table-datatables">
            <div class="card-panel">

                <div class="row" ng-show="formView">
                    <h4 class="header2">Add New Category</h4>
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input placeholder="Input Category Name" ng-model="formData.name" type="text">
                                <label>Name</label>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea placeholder="" class="materialize-textarea" ng-model="formData.description" ></textarea>
                                <label for="message">Description</label>
                            </div>
                        </div>
                        <div class="row">
                                <div class="input-field" style="right: 10px;">
                                    <button class="btn red waves-effect waves-light right" ng-click="CloseForm()" type="submit" >Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                                <div class="input-field" style="right: 20px;">
                                    <button class="btn cyan waves-effect waves-light right" ng-click="SubmitForm()" type="submit" >Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
            <div  ng-show="dataListView">
                <h4 class="header">Category List</h4>

                <table class="responsive-table display stripped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="x in categoryList">
                            <td>{{ x.name }}</td>
                            <td>{{ x.description }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;" ng-show="floatShow">
            <a class="btn-floating btn-large">
                <i class="mdi-action-stars"></i>
            </a>
            <ul>
                <li><a ng-click="ShowForm()" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
            </ul>
        </div>
        <!-- Floating Action Button -->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<?php
include 'footer.php';
?>

<script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>
<script>
    var myApp = angular.module('MaintenanceApp', []);
    //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
    myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location,$window) {

        $scope.formView = false;
        $scope.floatShow = true;
        $scope.dataListView = true;
        $scope.absUrl = $location.absUrl();
        //console.log($scope.absUrl);
        $scope.LogOut = function() {
            $http.post('apis/logout.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                $scope.errorMessage = response.data.Message;
                $window.location.href = '/Login.php';
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };
        $scope.formData = {
            name: "",
            description: "",
            id: 0
        };
        $scope.ShowForm = function() {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
        };

        $scope.CloseForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
        };

        $scope.SubmitForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            //console.log($scope.formData);
        };

        $scope.getCategory = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetCategoryList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.categoryList = response.data.Response;
                console.log($scope.categoryList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getCategory();

        $scope.$watch('op', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });
        $scope.$watch('type', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });

    });
</script>
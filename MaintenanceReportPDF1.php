<?php

include('connection/connection.php');

if (isset($_GET['m'])) {
    $maintenanceId = $_GET['m'];
    $formId = $_GET['f'];
} else {
    $maintenanceId = 1;
    $formId = 1;
}


//$formId = $json['form_id'];


$getMaintenanceSheetQuery = "SELECT * FROM maintenance_sheet where maintenance_form_id='$maintenanceId'";

$result = mysqli_query($con, $getMaintenanceSheetQuery);
$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";
$mainSheetList = array();
if ($result) {
    $count = mysqli_num_rows($result);
    //$mainSheetList = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $sheet_id = $row['id'];
        $formDetail = GetSheetDetail($con, $formId, $maintenanceId, $sheet_id);
        $formDetail['sheet_id']  = $sheet_id;
        $formDetail['maintenance_id']  = $maintenanceId;
        $formDetail['form_id']  = $formId;
        $formDetail['schedule_on']  = $row['schedule_on'];
        $formDetail['remarks']  = $row['remarks'];
        $formDetail['status']  = $row['status'];
        $formDetail['submitted_on']  = $row['submitted_on'];
        $formDetail['completed_on']  = $row['completed_on'];
        array_push($mainSheetList, $formDetail);
    }
    if ($count == 0) {
        $formDetail = GetSheetDetail($con, $formId, $maintenanceId, null);
        array_push($mainSheetList, $formDetail);
    }
    $ResponseObject->Response = $mainSheetList;
}

function GetSheetDetail($con, $formId, $maintenanceId, $sheet_id)
{
    $getFormQuery = "SELECT * FROM form where id='$formId'";
    $result = mysqli_query($con, $getFormQuery);
    // Get the data
    $customInfoList = array();
    $formFieldList = array();
    if ($result) {
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_assoc($result)) {
            //$CategoryList[$i]['id']  = $row['id'];
            //$CategoryList[$i]['name']  = mb_convert_encoding($row['name'], "UTF-8");
            $catId = $row['category_id'];
            $getCategoryName = "SELECT `name` FROM category where id='$catId'";
            $result = mysqli_query($con, $getCategoryName);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                //print_r($rowResult);
                $row['category']  = $rowResult['name'];
            }
            $formDetail = $row;
            $getFormFieldQry = "SELECT * FROM form_field where form_id='$formId' and `type` != 'Custom'";
            $result = mysqli_query($con, $getFormFieldQry);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                $formFieldId = $rowResult['id'];
                $getFieldValueQry = "SELECT * FROM maintenance_data where form_id='$formId' and `maintenance_form_id` = '$maintenanceId' and form_field_id='$formFieldId' and maintenance_sheet_id='$sheet_id'";
                $result1 = mysqli_query($con, $getFieldValueQry);
                while ($rowResult1 = mysqli_fetch_assoc($result1)) {
                    //print_r($rowResult1);
                    $rowResult['value'] = $rowResult1['value'];
                    $rowResult['added_on'] = $rowResult1['added_on'];
                }
                array_push($formFieldList, $rowResult);
            }

            $getCustomInfoQry = "SELECT * FROM form_field where form_id='$formId' and `type` = 'Custom'";
            $result = mysqli_query($con, $getCustomInfoQry);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                $formFieldId = $rowResult['id'];
                $getCustomInfoValueQry = "SELECT * FROM maintenance_data where form_id='$formId' and `maintenance_form_id` = '$maintenanceId' and form_field_id='$formFieldId'";
                $result1 = mysqli_query($con, $getCustomInfoValueQry);
                while ($rowResult1 = mysqli_fetch_assoc($result1)) {
                    //print_r($rowResult1);
                    $rowResult['value'] = $rowResult1['value'];
                }
                array_push($customInfoList, $rowResult);
            }



            $formDetail['formFields'] = $formFieldList;
            $formDetail['formCustomInfos'] = $customInfoList;
            return $formDetail;
        }
    }
}
//phpinfo();
//$json = json_encode($CategoryList,JSON_FORCE_OBJECT);
$json = json_encode($ResponseObject);
//echo $json;

$html2='<html>
<head>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}

.verticalTableHeader {
    text-align:center;
    white-space:nowrap;
    g-origin:50% 50%;
    -webkit-transform: rotate(90deg);
    -moz-transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -o-transform: rotate(90deg);
    transform: rotate(90deg);
    
}
.verticalTableHeader p {
    margin:0 -100% ;
    display:inline-block;
}
.verticalTableHeader p:before{
    content:"";
    width:0;
    padding-top:110%;
    display:inline-block;
    vertical-align:middle;
}
</style>
</head>
<body><div>
<blockquote><b>Maintenance Schedule/Data List</b> <a class="btn-floating waves-effect waves-light" ng-click="CloseScheduleList()"><i class="mdi-content-clear"></i></a>
    <a class="btn-floating waves-effect waves-light" ng-click="PDFReport()"><i class="mdi-content-clear"></i></a></blockquote>
<b>Name: <i>'.$mainSheetList[0]['name'].'</i></b><br>
<b>Description: <i>'.$mainSheetList[0]['description'].'</i></b><br>
<b>Category: <i>'.$mainSheetList[0]['category'].'</i></b><br>';

foreach($mainSheetList[0]['formCustomInfos'] as $value){
    $html2 = $html2.'<b>'.$value['description'].':<i>'.$value['value'].'</i><br></b>';

}
$html2 = $html2.'

</div>
<table id="customers">
<thead>
    <tr>
        <th>Schedule On</th>
        <th>Status</th>
        <th>Completed On</th>';

        foreach($mainSheetList[0]['formFields'] as $value){
            $html2 = $html2.'<th>'.$value['description'].'</th>';
        
        }
$html2 = $html2.'       
    </tr>
</thead>
<tbody>';
foreach($mainSheetList as $value){
    $html2 = $html2.'<tr>
    <td>'.$value['schedule_on'].'</td>
    <td>'.$value['status'].'</td>
    <td>'.$value['completed_on'].'</td>';
    foreach($value['formFields'] as $fieldValue){
        if (isset($fieldValue['value'])) {
            $html2 = $html2.'<td>'.$fieldValue['value'].'</td>';
        } else {
            $html2 = $html2.'<td></td>';
        }
    }
    $html2 = $html2.'<tr>';
}
echo  $html2 = $html2.'</tbody>
</table>
</body>
</html>';

$html = '<html>
<head>
<style>
#customers {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#customers td, #customers th {
  border: 1px solid #ddd;
  padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}

#customers th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #4CAF50;
  color: white;
}
</style>
</head>
<body>

<table id="customers">
  <tr>
    <th>Company</th>
    <th>Contact</th>
    <th>Country</th>
  </tr>
  <tr>
    <td>Alfreds Futterkiste</td>
    <td>Maria Anders</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Berglunds snabbköp</td>
    <td>Christina Berglund</td>
    <td>Sweden</td>
  </tr>
  <tr>
    <td>Centro comercial Moctezuma</td>
    <td>Francisco Chang</td>
    <td>Mexico</td>
  </tr>
  <tr>
    <td>Ernst Handel</td>
    <td>Roland Mendel</td>
    <td>Austria</td>
  </tr>
  <tr>
    <td>Island Trading</td>
    <td>Helen Bennett</td>
    <td>UK</td>
  </tr>
  <tr>
    <td>Königlich Essen</td>
    <td>Philip Cramer</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Laughing Bacchus Winecellars</td>
    <td>Yoshi Tannamuri</td>
    <td>Canada</td>
  </tr>
  <tr>
    <td>Magazzini Alimentari Riuniti</td>
    <td>Giovanni Rovelli</td>
    <td>Italy</td>
  </tr>
  <tr>
    <td>North/South</td>
    <td>Simon Crowther</td>
    <td>UK</td>
  </tr>
  <tr>
    <td>Paris spécialités</td>
    <td>Marie Bertrand</td>
    <td>France</td>
  </tr>
</table>

</body>
</html>';
//echo $html;
//==============================================================
//==============================================================
//==============================================================
require_once 'lib/vendor/autoload.php';
//include("lib/mpdf/Mpdf.php");
$mpdf = new \Mpdf\Mpdf();
//$mpdf->WriteHTML($html);
//$mpdf->Output();
exit;
//==============================================================
//==============================================================
//==============================================================

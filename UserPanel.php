<?php
include 'header.php';
?>
<section id="content">
    <div class="container" ng-controller="panelCtrl">
        <div class="row">
            <div class="col s12">
                <div class="card-panel" ng-show="userDetailShow">
                    <h5 class="header">User Detail</h5>
                    <b class="header">{{userDetail.full_name}}</b><br>
                    <b class="header">{{formDetail.designation}}</b><br>
                    <b class="header">Email: {{userDetail.email}}</b><br>
                    <b class="header">Company: {{userDetail.company.full_name}}</b>
                    <ul class="collection with-header">
                        <li class="collection-header">
                            <b>Basic Informations</b>
                        </li>
                        <li class="collection-item">
                            <blockquote><b>Mobile: {{userDetail.mobile_no}}</b></blockquote>
                            <blockquote><b>Address: {{userDetail.address}}</b></blockquote>
                            <blockquote><b>Type: {{userDetail.type}}</b></blockquote>
                            <blockquote><b>Last Login At: {{userDetail.last_login_at}}</b></blockquote>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="input-field" style="right: 10px;">
                            <button class="btn red waves-effect waves-light right" ng-click="CloseDetail()">Close
                                <i class="mdi-navigation-cancel right"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div ng-show="formView">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <h4>{{formTag}}</h4><br>
                            </div>
                        </div>
                        <ul class="collection with-header yellow darken-4">
                            <li class="collection-header">
                                <h6><b></b></h6>
                            </li>
                            <li class="collection-item">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="userForm.email" type="text">
                                        <label>User Mail</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="userForm.full_name" type="text">
                                        <label>Full Name</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="userForm.designation" type="text">
                                        <label style="color:black">Designation</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <label>Type</label><br>
                                        <select ng-model="userForm.type">
                                            <option value="" disabled selected>Choose User Type</option>
                                            <option value="Admin">Admin</option>
                                            <option value="Superviser">Superviser</option>
                                            <option value="User">User</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="userForm.password" type="password">
                                        <label>Password</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="userForm.address" type="text">
                                        <label>Address</label>
                                    </div>
                                    <div class="input-field col s12">
                                        <input placeholder="" ng-model="userForm.mobile_no" type="text">
                                        <label>Mobile No</label>
                                    </div>
                                </div>
                            </li>
                        </ul>

                        <div class="row">
                            <div class="input-field" style="right: 10px;">
                                <button class="btn red waves-effect waves-light right" ng-click="CloseForm()">Cancel
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div>
                            <div class="input-field" style="right: 20px;">
                                <button class="btn cyan waves-effect waves-light right" ng-click="SubmitUserForm()">Submit
                                    <i class="mdi-content-send right"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div ng-show="dataListView">
                    <h4 class="header">Form List</h4>
                    <!--table class="responsive-table display stripped" cellspacing="0"-->
                    <table class="stripped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Category</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="x in userList">
                                <td>{{ x.full_name }}</td>
                                <td>{{ x.email }}</td>
                                <td>{{ x.designation}}</td>
                                <td>
                                    <button class="btn cyan waves-effect waves-light" style="right: 10px;" ng-click="EditUserForm(x)"><i class="mdi-content-send right"></i>Edit</button>
                                    <button class="btn green waves-effect waves-light" style="right: 10px;" ng-click="GetUserDetail(x)"><i class="mdi-content-send right"></i>Detail</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;" ng-show="floatShow">
            <a class="btn-floating btn-large" ng-click="ShowUserForm()">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        <!-- Floating Action Button -->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<?php
include 'footer.php';
?>

<script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>



<script>
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 5, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd'
    });
    var myApp = angular.module('MaintenanceApp', ['ui.materialize']);
    //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
    myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location,$window) {
        $scope.formView = false;
        $scope.floatShow = true;
        $scope.dataListView = true;

        $scope.absUrl = $location.absUrl();
        //console.log($scope.absUrl);
        $scope.LogOut = function() {
                $http.post('apis/logout.php?', JSON.stringify($scope.formData)).then(function(response) {
                    console.log(response.data);
                    $scope.errorMessage = response.data.Message;
                    $window.location.href = '/Login.php';
                }, function(response) {
                    $scope.msg = "Service not Exists";
                    $scope.statusval = response.status;
                    $scope.statustext = response.statusText;
                    $scope.headers = response.headers();
                });

            };

        $scope.formFieldList = [{
            description: '',
            values: '',
            type: '',
            positive_value: '',
            negative_value: ''
        }];

        $scope.formCustomInfoList = [{
            description: '',
            info_type: 'custom'
        }];

        $scope.formFieldListInDb = $scope.formFieldList;

        $scope.ResetForm = function() {
            $scope.userForm = {
                full_name: "",
                address: "",
                password: "",
                mobile_no: "",
                type: "",
                email: "",
                id: 0
            };
        }

        $scope.ResetForm();

        $scope.ShowForm = function() {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.userDetailShow = false;
        };

        $scope.CloseForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.userDetailShow = false;
            $scope.ResetForm();
        };

        $scope.CloseDetail = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.userDetailShow = false;
        };

        $scope.GetUserDetail = function(value) {
            $scope.formView = false;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.userDetailShow = true;
            $scope.userDetail = value;
        };

        $scope.ShowUserForm = function(value) {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.userDetailShow = false;
            $scope.formTag = "Add New User";
            //$scope.userForm = ;
        };

        $scope.EditUserForm = function(value) {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.userDetailShow = false;
            $scope.userForm = value;
            $scope.formTag = "Edit User";
        };


        $scope.getUserList = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetUserList.php?'
            }).then(function(response) {
                console.log(response.data, response.status);
                $scope.userList = response.data.Response;
                console.log($scope.userList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getUserList();

        $scope.SubmitUserForm = function() {
            $scope.scheduleFormView = false;
            $scope.scheduleListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            $scope.userForm.tag = $scope.formTag==="Add New User" ? "add" : "update";

            $http.post('apis/AddUpdateUser.php?', JSON.stringify($scope.userForm)).then(function(response) {
                console.log(response.data);
                if (response.data.IsSuccess === true) {
                    $scope.CloseForm();
                    $scope.GetUserList();
                    //$scope.getFormList();
                }
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };


        $scope.$watch('op', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });
        $scope.$watch('type', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });

    });
</script>
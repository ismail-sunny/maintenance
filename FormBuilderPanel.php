<?php
include 'header.php';
?>
<section id="content">
    <div class="container" ng-controller="panelCtrl">
        <div class="row">
            <div class="col s12">
                <div id="table-datatables">
                    <div class="col s12 m8 l9 card-panel" ng-show="formDetailShow">
                        <h5 class="header">Form Detail</h5>
                        <b class="header">{{formDetail.name}}</b><br>
                        <b class="header">{{formDetail.description}}</b><br>
                        <b class="header">Category: {{formDetail.category}}</b>
                        <ul class="collection with-header">
                            <li class="collection-header">
                                <b>Basic Informations</b>
                            </li>
                            <li class="collection-item" ng-repeat="customInfo in formDetail.formCustomInfos">
                                <blockquote><b>{{$index+1}} {{customInfo.description}}</b></blockquote>
                            </li>
                        </ul>

                        <ul class="collection with-header">
                            <li class="collection-header">
                                <b>Check Lists</b>
                            </li>
                            <li class="collection-item" ng-repeat="formField in formDetail.formFields">
                                <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                                <p>Type : {{formField.type}}
                                    <br> Values: {{formField.selection_values}}
                                    <br> Positive Value: {{formField.positive_value}}
                                    <br> Negative Value: {{formField.negative_value}}
                                </p>
                            </li>
                        </ul>
                        <div class="row">
                            <div class="input-field" style="right: 10px;">
                                <button class="btn red waves-effect waves-light right" ng-click="CloseFormDetail()">Close
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-panel" ng-show="formView">
                        <h4 class="header2">Build New Form</h4>
                        <form class="col s12">
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="Input Form Name" ng-model="formData.name" type="text">
                                    <label>Name</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea placeholder="" class="materialize-textarea" ng-model="formData.description"></textarea>
                                    <label>Description</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <label>Select Category</label><br>
                                    <select class="browser-default" ng-model="selectedCategory" material-select>
                                        <option value="" disabled selected>Choose your option</option>
                                        <option ng-repeat="value in categoryList" value="{{value.id}}">{{value.name}}</option>
                                    </select>
                                </div>
                            </div>

                            <ul class="collection with-header yellow darken-4">
                                <li class="collection-header">
                                    <h6><b>Form Custom Information</b></h6>
                                </li>
                                <li class="collection-item" ng-repeat="formField in formCustomInfoList">
                                    <blockquote><b>Number : {{$index+1}}</b> <a class="btn-floating waves-effect waves-light" ng-click="formCustomInfoList.splice($index, 1)"><i class="mdi-content-clear"></i></a></blockquote>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <input placeholder="" ng-model="formField.description" type="text">
                                            <label>Checking info/Question</label>
                                        </div>
                                    </div>
                                    <!--div class="row" style="right: 10px;">
                                <button class="btn red waves-effect waves-light right" ng-click="formCustomInfoList.splice($index, 1)" >Remove
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div-->
                                </li>
                                <li class="collection-item">
                                    <div class="input-field" style="right: 20px;bottom:10px">
                                        <button class="btn cyan waves-effect waves-light right" ng-click="AddFormCustomInfo(0)">Add More
                                            <i class="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                    <br>
                                </li>
                            </ul>
                            <ul class="collection with-header green accent-4">
                                <li class="collection-header">
                                    <h6><b>Questionaries/Check List</b></h6>
                                </li>
                                <li class="collection-item" ng-repeat="formField in formFieldList">
                                    <blockquote><b>Number : {{$index+1}}</b> <a class="btn-floating waves-effect waves-light" ng-click="formFieldList.splice($index, 1)"><i class="mdi-content-clear"></i></a></blockquote>
                                    <div class="row">
                                        <div class="input-field col s12">
                                            <textarea placeholder="" class="materialize-textarea" ng-model="formField.description"></textarea>
                                            <label>Checking info/Question</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <select ng-model="formField.type" class="browser-default">
                                                <option value="Selection">Selection</option>
                                                <option value="Text">Text</option>
                                                <option value="Number">Number</option>
                                            </select>
                                            <label></label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input placeholder="Selection Values" ng-model="formField.values" type="text">
                                            <label>Selection Values with Comma/Pipe seperated</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="input-field col s6">
                                            <input placeholder="Positive Value" ng-model="formField.positive_value" type="text">
                                            <label>Positive value</label>
                                        </div>
                                        <div class="input-field col s6">
                                            <input placeholder="Negative Value" ng-model="formField.negative_value" type="text">
                                            <label>Negative Value</label>
                                        </div>
                                    </div>
                                    <!--div class="row" style="right: 10px;">
                                <button class="btn red waves-effect waves-light right" ng-click="formFieldList.splice($index, 1)" >Remove
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div-->
                                </li>
                                <li class="collection-item">
                                    <div class="input-field" style="right: 20px;bottom:10px">
                                        <button class="btn cyan waves-effect waves-light right" ng-click="AddFormField(0)">Add More
                                            <i class="mdi-content-send right"></i>
                                        </button>
                                    </div>
                                    <br>
                                </li>
                            </ul>


                            <div class="row">
                                <div class="input-field" style="right: 10px;">
                                    <button class="btn red waves-effect waves-light right" ng-click="CloseForm()">Cancel
                                        <i class="mdi-navigation-cancel right"></i>
                                    </button>
                                </div>
                                <div class="input-field" style="right: 20px;">
                                    <button class="btn cyan waves-effect waves-light right" ng-click="SubmitForm()">Submit
                                        <i class="mdi-content-send right"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div ng-show="dataListView">
                        <h4 class="header">Form List</h4>
                        <!--table class="responsive-table display stripped" cellspacing="0"-->
                        <table class="stripped">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="x in formList">
                                    <td>{{ x.name }}</td>
                                    <td>{{ x.description }}</td>
                                    <td>{{ x.category}}</td>
                                    <td>
                                        <button class="btn cyan waves-effect waves-light" style="right: 10px;"><i class="mdi-content-send right"></i>Edit</button>
                                        <button class="btn green waves-effect waves-light" style="right: 10px;" ng-click="GetFormDetail(x.id)"><i class="mdi-content-send right"></i>Detail</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;" ng-show="floatShow">
            <a class="btn-floating btn-large" ng-click="ShowForm()">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        <!-- Floating Action Button -->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<?php
include 'footer.php';
?>

<script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>



<script>
    var myApp = angular.module('MaintenanceApp', ['ui.materialize']);
    //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
    myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location,$window) {

        $scope.formView = false;
        $scope.floatShow = true;
        $scope.dataListView = true;
        $scope.absUrl = $location.absUrl();
        //console.log($scope.absUrl);
        $scope.LogOut = function() {
            $http.post('apis/logout.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                $scope.errorMessage = response.data.Message;
                $window.location.href = '/Login.php';
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };
        $scope.formFieldList = [{
            description: '',
            values: '',
            type: '',
            positive_value: '',
            negative_value: ''
        }];

        $scope.formCustomInfoList = [{
            description: '',
            info_type: 'custom'
        }];

        $scope.formFieldListInDb = $scope.formFieldList;

        $scope.ResetForm = function() {
            $scope.formFieldList = [{
                description: '',
                values: '',
                type: '',
                positive_value: '',
                negative_value: ''
            }];

            $scope.formCustomInfoList = [{
                description: '',
                info_type: 'custom'
            }];
        }

        $scope.AddFormCustomInfo = function(value) {
            if (value === 0) {
                $scope.formCustomInfoList.push({
                    description: '',
                    info_type: 'custom'
                });
            } else {
                $scope.formCustomInfoListInDb.push({
                    description: '',
                    info_type: 'custom'
                });
            }
        }


        $scope.AddFormField = function(value) {
            if (value === 0) {
                $scope.formFieldList.push({
                    description: '',
                    values: '',
                    type: '',
                    positive_value: '',
                    negative_value: ''
                });
            } else {
                $scope.formFieldListInDb.push({
                    description: '',
                    values: '',
                    type: '',
                    positive_value: '',
                    negative_value: '',
                    Edit: 1
                });
            }
        }

        $scope.formData = {
            name: "",
            description: "",
            id: 0
        };
        $scope.ShowForm = function() {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.formDetailShow = false;
        };

        $scope.CloseForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
        };

        $scope.CloseFormDetail = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
        };

        $scope.getFormList = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetFormList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.formList = response.data.Response;
                console.log($scope.formList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getFormList();

        $scope.getCategory = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetCategoryList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.categoryList = response.data.Response;
                console.log($scope.categoryList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getCategory();

        $scope.GetFormDetail = function(value) {
            var data = {
                form_id: value
            };
            $http.post('apis/GetFormDetail.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data.Response);
                $scope.formDetail = response.data.Response;
                $scope.formView = false;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.formDetailShow = true;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };


        $scope.SubmitForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            console.log($scope.selectedCategory);
            console.log($scope.formData);
            var data = {
                tag: "add",
                name: $scope.formData.name,
                description: $scope.formData.description,
                category_id: $scope.selectedCategory,
                formCustomInfos: $scope.formCustomInfoList,
                formFields: $scope.formFieldList
            };
            $http.post('apis/addUpdateForm.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data);
                if (response.data === 'success') {
                    $scope.ResetForm();
                    $scope.CloseForm();
                    $scope.getFormList();
                }
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
            /*if ($scope.formData.name) {
                $http({
                    method: 'POST',
                    url: 'apis/addUpdateForm.php?',
                    headers: {
                        "Accept": "application/json;charset=utf-8",
                        'Content-Type': false
                    },
                    data: {
                        name: $scope.formData.name,
                        description: $scope.formData.description,
                        category_id: $scope.selectedCategory,
                        formCustomInfos: $scope.formCustomInfoList,
                        formFields: $scope.formFieldList
                    },
                    transformRequest: function(data, headersGetter) {
                        var formData = new FormData();
                        angular.forEach(data, function(value, key) {
                            formData.append(key, value);
                        });
                        //var headers = headersGetter();
                        //delete headers['Content-Type'];
                        return formData;
                    }
                }).then(function(response) { // on success
                    console.log(response.data);
                    $scope.addProductResult = response.data;
                    if ($scope.addProductResult === 'success') {
                        $scope.getFormList();
                    }
                }, function(response) {
                    //on error;
                });
            } else {
                $scope.loadingR = false;
                $scope.error = true;
            }*/
        };



        $scope.$watch('op', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });
        $scope.$watch('type', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });

    });
</script>
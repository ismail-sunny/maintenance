<?php
include 'header.php';
?>
<section id="content">
    <div class="container" ng-controller="panelCtrl">
        <div class="row">
            <div class="col s12">
                <ul id="task-card" class="collection with-header" ng-show="scheduleListView">
                    <li class="collection-header cyan">
                        <h4 class="task-card-title">My Task</h4>
                        <p class="task-card-date">March 26, 2015</p>
                    </li>
                    <a href="#task-modal" class="task-add modal-trigger btn-floating waves-effect waves-light"><i class="mdi-content-add"></i></a>
                    <li class="collection-item dismissable" ng-repeat="x in maintenanceScheduleList">
                        <input type="checkbox" ng-checked="x.name=='Transformer'" disabled="disabled" />
                        <label>{{x.name}}({{x.description}})<br><span ng-repeat="y in x.formCustomInfos">{{y.description}} : {{y.value}}<br></span><a href="#" class="secondary-content"><span class="ultra-small">{{x.schedule_on}}</span></a>
                        </label>
                        <span ng-class="x.status == 'New2' ? 'task-cat teal' : 'task-cat red'"> {{x.status}}</span>
                        <span><a class="btn-floating btn-flat waves-effect waves-light green accent-2 white-text" ng-click="ShowForm(x)"><i class="mdi-action-receipt"></i></a></span>
                    </li>
                </ul>
                <div ng-show="scheduleFormView">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <h4>Maintenance Sheet Data</h4><br>
                                <b>Name: <i>{{scheduleForm.name}}</i></b><br>
                                <b>Description: <i>{{scheduleForm.description}}</i></b><br>
                                <b>Category: <i>{{scheduleForm.category}}</i></b><br>
                                <b ng-repeat="x in scheduleForm.formCustomInfos">{{x.description}} :<i>{{x.value}}</i><br></b>
                            </div>
                        </div>
                        <ul class="collection with-header">
                            <li class="collection-header">
                                <h6><b>Check List</b></h6>
                            </li>
                            <li ng-class="x.value===x.negative_value ? 'collection-item red lighten-4' : 'collection-item'" ng-repeat="x in scheduleForm.formFields">
                                <b><i>{{x.description}}</i></b><br>
                                <input ng-if="x.type !=='Selection'" ng-disabled="scheduleForm.status==='submit' || scheduleForm.status==='completed' " ng-model="x.value" type="text">
                                <p ng-if="x.type==='Selection'" ng-repeat="s in x.selection_values_array">
                                    <input name="{{x.description}}" ng-disabled="scheduleForm.status==='submit' || scheduleForm.status==='completed'" type="radio" ng-model="x.value" value="{{s}}" id="{{x.description}}-{{s}}" />
                                    <label for="{{x.description}}-{{s}}" style="color: black;">{{s}}</label>
                                </p>
                            </li>
                            <li class="collection-item">
                                <b>Remarks</b><br>
                                <input ng-model="scheduleForm.remarks" placeholder="Remarks" type="text">
                            </li>
                        </ul>
                        <div class="row">
                            <div class="input-field">
                                <button class="btn red waves-effect waves-light right" ng-click="CloseForm()">Cancel
                                    <i class="mdi-navigation-cancel right"></i>
                                </button>
                            </div>
                            <div class="input-field" style="right: 10px;">
                                <button class="btn cyan waves-effect waves-light right" ng-disabled="scheduleForm.status==='submit' || scheduleForm.status==='completed'" ng-click="SubmitMaintenanceScheduleForm(scheduleForm,'draft')">Draft
                                    <i class="mdi-content-send right"></i>
                                </button>
                            </div>
                            <div class="input-field" style="right: 20px;">
                                <button class="btn cyan waves-effect waves-light right" ng-disabled="scheduleForm.status==='submit' || scheduleForm.status==='completed'" ng-click="SubmitMaintenanceScheduleForm(scheduleForm,'submit')">Submit
                                    <i class="mdi-content-send right"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div id="task-modal" class="modal">
                    <nav class="task-modal-nav red">
                        <div class="nav-wrapper">
                            <div class="left col s12 m5 l5">
                                <ul>
                                    <li><a href="#!" class="todo-menu"><i class="modal-action modal-close  mdi-hardware-keyboard-backspace"></i></a>
                                    </li>
                                    <li><a href="#!" class="todo-add">Add</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col s12 m7 l7 hide-on-med-and-down">
                                <ul class="right">
                                    <li><a href="#!"><i class="mdi-editor-attach-file"></i></a>
                                    </li>
                                    <li><a href="#!"><i class="mdi-action-loyalty"></i></a>
                                    </li>
                                    <li><a href="#!"><i class="mdi-navigation-more-vert"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    <div class="modal-content">
                        <div class="row">
                            <form class="col s12">
                                <div class="row">
                                    <div class="input-field col s12">
                                        <input id="todo-title" type="text" class="validate">
                                        <label for="todo-title">Todo Title</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s6">
                                        <select>
                                            <option value="" disabled selected>Choose your option</option>
                                            <option value="1">Design</option>
                                            <option value="2">Develop</option>
                                            <option value="3">Testing</option>
                                        </select>
                                    </div>
                                    <div class="input-field col s6">
                                        <div class="file-field input-field">
                                            <input class="file-path validate" type="text" />
                                            <div class="btn">
                                                <span>File</span>
                                                <input type="file" />
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="input-field col s12">
                                        <textarea id="comments" class="materialize-textarea" length="500"></textarea>
                                        <label for="comments">Comments</label>
                                        <span class="character-counter"></span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;" ng-show="floatShow">
            <a class="btn-floating btn-large" ng-click="ShowScheduleForm(maintenanceSheetList[0])">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        <!-- Floating Action Button -->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<?php
include 'footer.php';
?>

<script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>



<script>
    $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 5, // Creates a dropdown of 15 years to control year
        format: 'yyyy-mm-dd'
    });
    var myApp = angular.module('MaintenanceApp', ['ui.materialize']);
    //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
    myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location,$window) {
        $scope.formView = false;
        $scope.floatShow = false;
        $scope.scheduleListView = true;

        $scope.absUrl = $location.absUrl();
        //console.log($scope.absUrl);
        $scope.LogOut = function() {
            $http.post('apis/logout.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                $scope.errorMessage = response.data.Message;
                $window.location.href = '/Login.php';
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };
        $scope.ResetForm = function() {
            $scope.formData = {
                name: "",
                description: "",
                id: 0
            };
        }

        $scope.ResetForm();

        $scope.ShowForm = function(value) {
            $scope.scheduleForm = value;
            $scope.scheduleFormView = true;
            $scope.scheduleListView = false;
        };

        $scope.CloseForm = function() {
            $scope.scheduleFormView = false;
            $scope.scheduleListView = true;
            $scope.ResetForm();
        };

        $scope.CloseFormDetail = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
        };

        $scope.CloseScheduleList = function() {
            $scope.mainDataListView = true;
            $scope.scheduleListView = false;
            $scope.floatShow = false;
        };



        $scope.GetFormDetail = function(value) {
            var data = {
                form_id: value
            };
            $http.post('apis/GetFormDetail.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data.Response);
                $scope.formDetail = response.data.Response;
                $scope.formView = false;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.formDetailShow = true;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };


        $scope.GetMaintenanceScheduleList = function(maintanenceId, formId) {
            var data = {
                maintenance_id: maintanenceId,
                form_id: formId
            };
            $http.post('apis/GetMaintenanceScheduleList.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data);
                $scope.maintenanceScheduleList = response.data.Response;
                //$scope.formView = true;
                //$scope.dataListView = false;
                //$scope.floatShow = false;
                //$scope.formDetailShow = false;
                //$scope.scheduleListView = true;
                //$scope.mainDataListView = false;
                //$scope.floatShow = true;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };
        $scope.GetMaintenanceScheduleList(null, null);



        $scope.SubmitMaintenanceScheduleForm = function(value, status) {
            $scope.scheduleFormView = false;
            $scope.scheduleListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            $scope.formData = value;
            $scope.formData.tag = "submit";
            $scope.formData.status = "submit";
            console.log($scope.formData);

            $http.post('apis/UpdateScheduleForm.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                if (response.data === 'success') {
                    $scope.CloseForm();
                    $scope.GetMaintenanceScheduleList(null, null);
                    //$scope.getFormList();
                }
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };


        $scope.$watch('op', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });
        $scope.$watch('type', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });

    });
</script>
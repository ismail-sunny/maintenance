<?php
include('../connection/connection.php');

$raw_data = file_get_contents('php://input');
$json = json_decode($raw_data, true);
//foreach ($json as $data) {
    //print_r($data);
//}

//$maintenanceId = $json['maintenance_id'];
//$formId = $json['form_id'];


$getUserQuery = "SELECT * FROM user";

$result = mysqli_query($con, $getUserQuery);
$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";

if ($result) {
    $count = mysqli_num_rows($result);
    $userList = array();
    while ($row = mysqli_fetch_assoc($result)) {
        //print_r($row);
        $user_id = $row['id'];
        $user = $row;
        $user['password'] = "";
        $user['company'] = GetParentCompanyDetail($con,$row['company_id']);
        $user['parent'] = GetParentCompanyDetail($con,$row['parent_id']);
        //$user['schedule_on']  = $row['schedule_on'];
        array_push($userList, $user);
    }
    if ($count == 0) {
        $ResponseObject->IsSuccess = false;
        $ResponseObject->Message = "Error";
        $ResponseObject->Response = "No User Available";
        $json = json_encode($ResponseObject);
        echo $json;
        exit;
    }
    $ResponseObject->Response = $userList;
}

function GetParentCompanyDetail($con, $parentId)
{
    $getQuery = "SELECT * FROM user where id='$parentId'";
    $result = mysqli_query($con, $getQuery);
    if ($result) {
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_assoc($result)) {
            return $row;
        }
    }
}
//$json = json_encode($CategoryList,JSON_FORCE_OBJECT);
$json = json_encode($ResponseObject);
echo $json;
exit;

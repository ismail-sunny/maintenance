<?php
include('../connection/connection.php');
// Fetch content and determine boundary
$raw_data = file_get_contents('php://input');
//print_r($raw_data);
$mainData = json_decode($raw_data, true);
foreach ($mainData as $data) {
    //print_r($data);
}


$form_id = $mainData['id'];
$maintenance_form_id = $mainData['maintenance_form_id'];
$maintenance_sheet_id = $mainData['maintenance_sheet_id'];
$name = $mainData['name'];
$description = $mainData['description'];
$categoryId = $mainData['category_id'];
$status = $mainData['status'];

$formCustomInfos = $mainData['formCustomInfos'];
$formFields = $mainData['formFields'];
$tag = $mainData['tag'];

$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";


if ($tag == 'submit') {
    $updateMaintanenceSheetQry = "UPDATE maintenance_sheet SET status='$status',submitted_on=NOW(),is_submitted=1 where id='$maintenance_form_id'";
    $resultFormQry = mysqli_query($con, $updateMaintanenceSheetQry);
    if (!$resultFormQry) {
        //printf("Errormessage: %s\n", mysqli_error($con));
        $ResponseObject->IsSuccess = false;
        $ResponseObject->Message = "Error";
        $ResponseObject->Response = mysqli_error($con);
        echo json_encode($ResponseObject);
        exit;
    }
    foreach ($formFields as $key => $value) {
        $form_id = $value['form_id'];
        $field_id = $value['id'];
        $value = $value['value'];

        $checkExistingData = ChechExistingFormField($con, $maintenance_form_id, $field_id);
        if ($checkExistingData == 0) {
            $insertFormDataQry = "INSERT INTO maintenance_data (`value`,`maintenance_form_id`,`maintenance_sheet_id`,`form_field_id`,`form_id`,`added_on`) VALUES ('$value','$maintenance_form_id','$maintenance_sheet_id','$field_id','$form_id',NOW())";
            $resultQry = mysqli_query($con, $insertFormDataQry);
        }else{
            $updateDataQry = "UPDATE maintanence_data SET 'value'='$value' where id='$checkExistingData'";
            $resultQry = mysqli_query($con, $updateDataQry);
        }
        if (!$resultQry) {
            //printf("Errormessage: %s\n", mysqli_error($con));
            $ResponseObject->IsSuccess = false;
            $ResponseObject->Message = "Error";
            $ResponseObject->Response = mysqli_error($con);
            echo json_encode($ResponseObject);
            exit;
        }
    }
} else {
    //update
}

function ChechExistingFormField($con, $maintenanceId, $fieldId)
{
    $selectFormDataQry = "select from * maintenance_data where form_field_id='$fieldId'";
    $resultQry = mysqli_query($con, $selectFormDataQry);
    if ($resultQry) {
        $count = mysqli_num_rows($resultQry);
        if ($count == 0) {
            return 0;
        } else {
            while ($rowResult = mysqli_fetch_assoc($resultQry)) {
                return $rowResult[id];
            }
        }
    }
}

$json = json_encode($ResponseObject);
echo $json;

exit;

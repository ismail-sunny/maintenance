<?php
include('../connection/connection.php');
// Fetch content and determine boundary
$raw_data = file_get_contents('php://input');
//print_r($raw_data);
$mainData = json_decode($raw_data, true);
foreach ($mainData as $data) {
    //print_r($data);
}


$form_id = $mainData['id'];
$name = $mainData['name'];
$description = $mainData['description'];
$categoryId = $mainData['category_id'];

$formCustomInfos = $mainData['formCustomInfos'];
$formFields = $mainData['formFields'];
$tag = $mainData['tag'];



if ($tag == 'add') {
    $insertMaintanenceFormQry = "INSERT INTO maintenance_form (form_id, added_on) VALUES ('$form_id',NOW())";
    $resultFormQry = mysqli_query($con, $insertMaintanenceFormQry);
    if (!$resultFormQry) {
        printf("Errormessage: %s\n", mysqli_error($con));
        exit;
    }
    $maintanenceFormId = mysqli_insert_id($con);

    if ($maintanenceFormId) {

        foreach ($formCustomInfos as $key => $value) {
            $form_id = $value['form_id'];
            $field_id = $value['id'];
            $value = $value['value'];
            $insertFormDataQry = "INSERT INTO maintenance_data (`value`,`maintenance_form_id`,`form_field_id`,`form_id`,`added_on`) VALUES ('$value','$maintanenceFormId','$field_id','$form_id',NOW())";
            $resultQry = mysqli_query($con, $insertFormDataQry);
            if (!$resultQry) {
                printf("Errormessage: %s\n", mysqli_error($con));
                exit;
            }
        }

        echo 'success';
    } else {
        echo 'error';
    }
} else {
    //update
}

exit;

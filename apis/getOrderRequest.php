<?php
include('../connection/connection.php');
$getQry = "SELECT * FROM order_request";

$result = mysqli_query($con,$getQry);


// Get the data
$orderRequestList = array();

if ($result) {
    $count = mysqli_num_rows($result);
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $orderId= $row['id'];
        $orderRequestList[$i]['id']  = $row['id'];
        $orderRequestList[$i]['requesterName']  = mb_convert_encoding($row['requester_name'], "UTF-8");
        $orderRequestList[$i]['requesterEmail']  = mb_convert_encoding($row['requester_email'], "UTF-8");
        $orderRequestList[$i]['requesterPhone']  = $row['requester_phone_no'];        
        $orderRequestList[$i]['status']  = $row['status'];        
        $orderRequestList[$i]['message']  = $row['message'];        
        $orderRequestList[$i]['addedOn']  = $row['added_on']; 
        $orderRequestList[$i]['updatedOn']  = $row['updated_on']; 
        
        $getRequestProduct = "SELECT * FROM request_item AS a LEFT JOIN product AS b ON a.product_id=b.id LEFT JOIN `product_assets` AS c ON b.id=c.product_id 
        WHERE a.request_id='$orderId' GROUP BY a.product_id  order by a.id desc";
        $productResult = mysqli_query($con,$getRequestProduct);
        $j=1;
        while ($rowResult = mysqli_fetch_assoc($productResult)) {
            $orderRequestList[$i]['product'][$j]  = $rowResult; 
            $j++;
        }
        $getRequestStatusLog= "SELECT * FROM request_status_change_log as a left join users as b on a.changed_by=b.id where a.request_id='$orderId' order by a.id desc";
        $statusResult = mysqli_query($con,$getRequestStatusLog);
        $j=1;
        while ($rowResult = mysqli_fetch_assoc($statusResult)) {
            $orderRequestList[$i]['statusLog'][$j]  = $rowResult; 
            $j++;
        }
        $i++;
    }
}
$json = json_encode($orderRequestList,JSON_FORCE_OBJECT);
echo $json;
exit;

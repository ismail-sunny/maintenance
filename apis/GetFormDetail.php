<?php
include('../connection/connection.php');
$raw_data = file_get_contents('php://input');
$mainData = json_decode($raw_data, true);

$formId = $mainData['form_id'];

$getFormQuery = "SELECT * FROM form where id='$formId'";

$result = mysqli_query($con,$getFormQuery);


// Get the data
$customInfoList = array();
$formFieldList = array();

if ($result) {
    $count = mysqli_num_rows($result);
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        //$CategoryList[$i]['id']  = $row['id'];
        //$CategoryList[$i]['name']  = mb_convert_encoding($row['name'], "UTF-8");
        $catId = $row['category_id'];
        $getCategoryName = "SELECT `name` FROM category where id='$catId'";
        $result = mysqli_query($con,$getCategoryName);
        while ($rowResult = mysqli_fetch_assoc($result)) {
            //print_r($rowResult);
            $row['category']  = $rowResult['name']; 
        }
        $response = $row;
        $getFormFieldQry = "SELECT * FROM form_field where form_id='$formId' and `type` != 'Custom'";
        $result = mysqli_query($con,$getFormFieldQry);
        while ($rowResult = mysqli_fetch_assoc($result)) {
            array_push($formFieldList,$rowResult);
        }

        $getCustomInfoQry = "SELECT * FROM form_field where form_id='$formId' and `type` = 'Custom'";
        $result = mysqli_query($con,$getCustomInfoQry);
        while ($rowResult = mysqli_fetch_assoc($result)) {
            array_push($customInfoList,$rowResult);
        }
        $response['formFields'] = $formFieldList;
        $response['formCustomInfos'] = $customInfoList;
        $i++;
    }
}
//print_r($response);
//$json = json_encode($CategoryList,JSON_FORCE_OBJECT);
$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";
$ResponseObject->Response = $response;
$json = json_encode($ResponseObject);
echo $json;
exit;
?>


<?php
include('../connection/connection.php');

$raw_data = file_get_contents('php://input');
//print_r($raw_data);
$mainData = json_decode($raw_data, true);
//foreach ($mainData as $data) {
    //print_r($data);
//}

$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";

$email = $mainData['email'];
$password = $mainData['password'];

$selectQry = "SELECT * FROM user where email='$email'";

$result = mysqli_query($con, $selectQry);
$response = null;
if ($result) {
    $count = mysqli_num_rows($result);
    if ($count == 0) {
        $ResponseObject->IsSuccess = false;
        $ResponseObject->Message = "No Such User";
        echo json_encode($ResponseObject, JSON_FORCE_OBJECT);
        exit;
    }
    while ($row = mysqli_fetch_assoc($result)) {
        //if($row['password']== md5($password)){
        if ($row['password'] == md5($password)) {

            $response['isSuccess'] = true;
            $response['message'] = 'Successfull';
            $_SESSION['authenticated'] = 1;
            $_SESSION['email'] = $row['email'];
            //$_SESSION['password'] = $password;
            $_SESSION['user_id'] = $row['id'];
            $_SESSION['fullName'] = $row['full_name'];
            /*print_r($_SESSION);
            if(session_status()==PHP_SESSION_NONE){
                echo 'PHP_SESSION_NONE';
            }if(session_status()==PHP_SESSION_DISABLED ){
                echo 'PHP_SESSION_DISABLED';    
            }
            if(session_status()==PHP_SESSION_ACTIVE  ){
                echo 'PHP_SESSION_ACTIVE';    
            }*/
            $response['email'] = $row['email'];
            $response['id'] = $row['id'];
            $response['fullName'] = $row['full_name'];
            $response['authenticated'] = 1;
            if (session_status() == PHP_SESSION_NONE) {
                $response['session'] = 0;
            }
            if (session_status() == PHP_SESSION_DISABLED) {
                $response['session'] = 1;
            }
            if (session_status() == PHP_SESSION_ACTIVE) {
                $response['session'] = 2;
            }
            $ResponseObject->Response = $response;
        } else {
            $ResponseObject->IsSuccess = false;
            $ResponseObject->Message = "Password Mismatch";
        }
    }
} else {
    $ResponseObject->IsSuccess = false;
    $ResponseObject->Message = mysqli_error($con);
}
echo json_encode($ResponseObject, JSON_FORCE_OBJECT);
exit;

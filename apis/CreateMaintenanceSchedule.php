<?php
include('../connection/connection.php');
// Fetch content and determine boundary
$raw_data = file_get_contents('php://input');
//print_r($raw_data);
$mainData = json_decode($raw_data, true);
foreach ($mainData as $data) {
    //print_r($data);
}


$schedule_on = $mainData['schedule_on'];
$status = $mainData['status'];
$assign_to = $mainData['assign_to'];

$maintenance_id = $mainData['maintenance_id'];
$tag = $mainData['tag'];

$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";


if ($tag == 'add') {
    $insertMaintanenceFormQry = "INSERT INTO maintenance_sheet (maintenance_form_id,schedule_on,status,assigned_to, added_on) VALUES ('$maintenance_id','$schedule_on','$status','$assign_to',NOW())";
    $resultFormQry = mysqli_query($con, $insertMaintanenceFormQry);
    if (!$resultFormQry) {
        //printf("Errormessage: %s\n", mysqli_error($con));
        $ResponseObject->IsSuccess = false;
        $ResponseObject->Message = mysqli_error($con);
        echo json_encode($ResponseObject);
        exit;
    }
    $maintanenceScheduleId = mysqli_insert_id($con);
    $ResponseObject->Response = $maintanenceScheduleId;
} else {
    //update
}
$json = json_encode($ResponseObject);
echo $json;
exit;

<?php
include('../connection/connection.php');
// Fetch content and determine boundary
$raw_data = file_get_contents('php://input');
$boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));
// Fetch each part
$parts = array_slice(explode($boundary, $raw_data), 1);
$data = array();
$images = array();
$image_url='';      
$i = 0;
//print_r($raw_data);


foreach ($parts as $part) {
    // If this is the last part, break
    if ($part == "--\r\n") {
        break;
    }
    // Separate content from headers
    $part = ltrim($part, "\r\n");
    list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);

    // Parse the headers list
    $raw_headers = explode("\r\n", $raw_headers);
    $headers = array();
    foreach ($raw_headers as $header) {
        list($name, $value) = explode(':', $header);
        $headers[strtolower($name)] = ltrim($value, ' ');
    }

    // Parse the Content-Disposition to get the field name, etc.
    if (isset($headers['content-disposition'])) {
        $filename = null;
        preg_match(
            '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
            $headers['content-disposition'],
            $matches
        );
        list(, $type, $name) = $matches;
        isset($matches[4]) and $filename = $matches[4];
        //print_r($type);

        
        // handle your fields here
        switch ($name) {
            // this is a file upload
            case 'userfile':
                file_put_contents($filename, $body);
                break;
            case 'upload':
                //print_r($body);
                if(strlen($body)>10){
                    $body = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $body));
                    //print_r($body);
                    $filename = $data['name'].".jpg";
                    $directory="../brands/".$filename;
                    $image_url = "brands/".$filename;
                    if (file_exists($directory)) { unlink ($directory); }
                    file_put_contents($directory, $body);  
                }  

                /*$body = trim($body);
                if ($body != "undefined") {
                    $imageBody = $body; 
                    $filename = $data['name'].".jpg";
                    $directory="../brands/".$filename;
                    $image_url = "brands/".$filename;
                    if (file_exists($directory)) { unlink ($directory); }
                    file_put_contents($directory, $body);       
                }*/
                break;
            // default for all other files is to populate $data
            default:
                $data[$name] = substr($body, 0, strlen($body) - 2);
                break;
        }
    }
}
$name = $data['name'];
$description = $data['description'];
$brandId = $data['id'];
$addOrUpdateBrand = $data['addOrUpdateBrand'];

if($addOrUpdateBrand=='add'){
    if($image_url!=''){
        $insertBrandQry = "INSERT INTO brand (name, description, image_url) VALUES ('$name','$description','$image_url')";
    }else{
        $insertBrandQry = "INSERT INTO brand (name, description) VALUES ('$name','$description')";        
    }
    //echo $insertBrandQry;

    $resultAddBrand = mysqli_query($con,$insertBrandQry);
    if(!$resultAddBrand){
        printf("Errormessage: %s\n", mysqli_error($con));
        exit;
    }
    $brandId = mysqli_insert_id($con);
    if($brandId){
        echo 'success';
    }else{
        echo 'error';
    }
}else{
    if($image_url!=''){
        $updateQry = "UPDATE brand set name='$name', description='$description', image_url='$image_url' where id='$brandId'";
    }else{
        $updateQry = "UPDATE brand set name='$name', description='$description' where id='$brandId'";        
    }//echo $updateQry;

    $resultUpdateBrand = mysqli_query($con,$updateQry);
    if($resultUpdateBrand){
        echo 'success';
    }else{
        printf("Errormessage: %s\n", mysqli_error($con));
    }

}
exit;
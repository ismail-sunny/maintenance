<?php
include('../connection/connection.php');

$raw_data = file_get_contents('php://input');
$json = json_decode($raw_data, true);
foreach ($json as $data) {
    //print_r($data);
}

//$maintenanceId = $json['maintenance_id'];
//$formId = $json['form_id'];


$getMaintenanceScheduleQuery = "SELECT * FROM `maintenance_sheet` AS a LEFT JOIN (SELECT id AS maintenance_form_id,form_id FROM `maintenance_form`) AS b ON a.maintenance_form_id=b.maintenance_form_id";

$result = mysqli_query($con, $getMaintenanceScheduleQuery);
$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";

if ($result) {
    $count = mysqli_num_rows($result);
    $mainSheetList = array();
    while ($row = mysqli_fetch_assoc($result)) {
        //print_r($row);
        $sheet_id = $row['id'];
        $formDetail = GetSheetDetail($con,  $row['form_id'], $row['maintenance_form_id'], $sheet_id);
        $formDetail['maintenance_sheet_id']  = $sheet_id;
        $formDetail['maintenance_form_id']  = $row['maintenance_form_id'];
        $formDetail['form_id']  = $row['form_id'];

        $formDetail['schedule_on']  = $row['schedule_on'];
        $formDetail['status']  = $row['status'];
        $formDetail['submitted_on']  = $row['submitted_on'];
        $formDetail['completed_on']  = $row['completed_on'];
        array_push($mainSheetList, $formDetail);
    }
    /*if ($count == 0) {
        $formDetail = GetSheetDetail($con, $formId, $maintenanceId);
        array_push($mainSheetList, $formDetail);
    }*/
    $ResponseObject->Response = $mainSheetList;
}

function GetSheetDetail($con, $formId, $maintenanceId, $sheet_id)
{
    $getFormQuery = "SELECT * FROM form where id='$formId'";
    $result = mysqli_query($con, $getFormQuery);
    // Get the data
    $customInfoList = array();
    $formFieldList = array();
    if ($result) {
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_assoc($result)) {
            //$CategoryList[$i]['id']  = $row['id'];
            //$CategoryList[$i]['name']  = mb_convert_encoding($row['name'], "UTF-8");
            $catId = $row['category_id'];
            $getCategoryName = "SELECT `name` FROM category where id='$catId'";
            $result = mysqli_query($con, $getCategoryName);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                //print_r($rowResult);
                $row['category']  = $rowResult['name'];
            }
            $formDetail = $row;
            $getFormFieldQry = "SELECT * FROM form_field where form_id='$formId' and `type` != 'Custom'";
            $result = mysqli_query($con, $getFormFieldQry);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                $formFieldId = $rowResult['id'];
                $rowResult['selection_values_array'] = explode(',', $rowResult['selection_values']);
                $getFieldValueQry = "SELECT * FROM maintenance_data where form_id='$formId' and `maintenance_form_id` = '$maintenanceId' and maintenance_sheet_id='$sheet_id' and form_field_id='$formFieldId'";
                $result1 = mysqli_query($con, $getFieldValueQry);
                while ($rowResult1 = mysqli_fetch_assoc($result1)) {
                    //print_r($rowResult1);
                    $rowResult['value'] = $rowResult1['value'];
                    $rowResult['added_on'] = $rowResult1['added_on'];
                }
                array_push($formFieldList, $rowResult);
            }

            $getCustomInfoQry = "SELECT * FROM form_field where form_id='$formId' and `type` = 'Custom'";
            $result = mysqli_query($con, $getCustomInfoQry);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                $formFieldId = $rowResult['id'];
                $getCustomInfoValueQry = "SELECT * FROM maintenance_data where form_id='$formId' and `maintenance_form_id` = '$maintenanceId' and form_field_id='$formFieldId'";
                $result1 = mysqli_query($con, $getCustomInfoValueQry);
                while ($rowResult1 = mysqli_fetch_assoc($result1)) {
                    //print_r($rowResult1);
                    $rowResult['value'] = $rowResult1['value'];
                }
                array_push($customInfoList, $rowResult);
            }



            $formDetail['formFields'] = $formFieldList;
            $formDetail['formCustomInfos'] = $customInfoList;
            return $formDetail;
        }
    }
}
//$json = json_encode($CategoryList,JSON_FORCE_OBJECT);
$json = json_encode($ResponseObject);
echo $json;
exit;

<?php
include('../connection/connection.php');
$getProductQry = "SELECT * FROM product";

$result = mysqli_query($con,$getProductQry);


// Get the data
$productList = array();

if ($result) {
    $count = mysqli_num_rows($result);
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        $productId= $row['id'];
        $productList[$i]['id']  = $row['id'];
        $productList[$i]['name']  = mb_convert_encoding($row['name'], "UTF-8");
        $productList[$i]['description']  = mb_convert_encoding($row['description'], "UTF-8");
        $productList[$i]['retailPrice']  = $row['retail_price'];        
        $productList[$i]['bundlePrice']  = $row['bundle_price'];        
        $productList[$i]['productStatus']  = $row['product_status'];        
        $productList[$i]['categoryId']  = $row['category_id']; 
        $productList[$i]['brandId']  = $row['brand_id']; 
        $catId=  $row['category_id'];
        $brandId=  $row['brand_id'];
        
        $getCategoryName = "SELECT `title` FROM category where id='$catId'";
        $categoryNameResult = mysqli_query($con,$getCategoryName);
        while ($rowResult = mysqli_fetch_assoc($categoryNameResult)) {
            //print_r($rowResult);
            $productList[$i]['category']  = $rowResult['title']; 
        }

        $getBrandName = "SELECT `name` FROM brand where id='$brandId'";
        $nameResult = mysqli_query($con,$getBrandName);
        while ($rowResult = mysqli_fetch_assoc($nameResult)) {
            //print_r($rowResult);
            $productList[$i]['brand']  = $rowResult['name']; 
        }

        $getProductAssetQry = "SELECT * FROM product_assets where product_id='$productId'";
        $assetResult = mysqli_query($con,$getProductAssetQry);
        $j=1;
        while ($rowResult = mysqli_fetch_assoc($assetResult)) {
            $productList[$i]['images'][$j]['type']  = $rowResult['type']; 
            $productList[$i]['images'][$j]['url']  = $rowResult['url']; 
            $j++;
        }
        $i++;
    }
}
$json = json_encode($productList,JSON_FORCE_OBJECT);
echo $json;
exit;

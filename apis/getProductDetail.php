<?php
include('../connection/connection.php');
if(!isset($_SESSION['productId'])){
    echo '';
    $_SESSION['productId']=0;
    exit;
}
$productId=$_SESSION['productId'];
$getProductQry = "SELECT * FROM product where id='$productId'";

$result = mysqli_query($con,$getProductQry);


// Get the data
$productDetail = array();
$count = mysqli_num_rows($result);

if ($count!=0) {
    while ($row = mysqli_fetch_assoc($result)) {
        $productId= $row['id'];
        $productDetail['id']  = $row['id'];
        $productDetail['name']  = mb_convert_encoding($row['name'], "UTF-8");
        $productDetail['description']  = mb_convert_encoding($row['description'], "UTF-8");
        $productDetail['retailPrice']  = $row['retail_price'];        
        $productDetail['bundlePrice']  = $row['bundle_price'];        
        $productDetail['productStatus']  = $row['product_status'];        
        $productDetail['categoryId']  = $row['category_id']; 
        $productDetail['brandId']  = $row['brand_id']; 
        $catId=  $row['category_id'];
        $brandId=  $row['brand_id'];
        
        $getCategoryName = "SELECT `title` FROM category where id='$catId'";
        $categoryNameResult = mysqli_query($con,$getCategoryName);
        while ($rowResult = mysqli_fetch_assoc($categoryNameResult)) {
            //print_r($rowResult);
            $productDetail['category']  = $rowResult['title']; 
        }

        $getBrandName = "SELECT `name` FROM brand where id='$brandId'";
        $nameResult = mysqli_query($con,$getBrandName);
        while ($rowResult = mysqli_fetch_assoc($nameResult)) {
            //print_r($rowResult);
            $productDetail['brand']  = $rowResult['name']; 
        }

        $getProductAssetQry = "SELECT * FROM product_assets where product_id='$productId'";
        $assetResult = mysqli_query($con,$getProductAssetQry);
        $j=1;
        while ($rowResult = mysqli_fetch_assoc($assetResult)) {
            $productDetail['images'][$j]['type']  = $rowResult['type']; 
            $productDetail['images'][$j]['url']  = $rowResult['url']; 
            $j++;
        }
    }
    $json = json_encode($productDetail,JSON_FORCE_OBJECT);
    echo $json;
}else{
    echo '';
}
exit;

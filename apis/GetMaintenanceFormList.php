<?php
include('../connection/connection.php');
$getFormQuery = "SELECT * FROM maintenance_form";

$result = mysqli_query($con, $getFormQuery);
$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";

if ($result) {
    $count = mysqli_num_rows($result);
    $i = 0;
    $mainFormList = array();
    while ($row = mysqli_fetch_assoc($result)) {
        $maintenanceId = $row['id'];
        $formId = $row['form_id'];
        $formDetail = GetFormDetail($con, $formId, $maintenanceId);
        $formDetail['maintenance_id']=$maintenanceId;
        $formDetail['form_id']=$formId;
        array_push($mainFormList, $formDetail);
        $i++;
    }
    $ResponseObject->Response = $mainFormList;
}

function GetFormDetail($con, $formId, $maintenanceId)
{
    $getFormQuery = "SELECT * FROM form where id='$formId'";
    $result = mysqli_query($con, $getFormQuery);
    // Get the data
    $customInfoList = array();
    $formFieldList = array();
    if ($result) {
        $count = mysqli_num_rows($result);
        while ($row = mysqli_fetch_assoc($result)) {
            //$CategoryList[$i]['id']  = $row['id'];
            //$CategoryList[$i]['name']  = mb_convert_encoding($row['name'], "UTF-8");
            $catId = $row['category_id'];
            $getCategoryName = "SELECT `name` FROM category where id='$catId'";
            $result = mysqli_query($con, $getCategoryName);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                //print_r($rowResult);
                $row['category']  = $rowResult['name'];
            }
            $formDetail = $row;
            $getFormFieldQry = "SELECT * FROM form_field where form_id='$formId' and `type` != 'Custom'";
            $result = mysqli_query($con, $getFormFieldQry);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                array_push($formFieldList, $rowResult);
            }

            $getCustomInfoQry = "SELECT * FROM form_field where form_id='$formId' and `type` = 'Custom'";
            $result = mysqli_query($con, $getCustomInfoQry);
            while ($rowResult = mysqli_fetch_assoc($result)) {
                $formFieldId = $rowResult['id'];
                $getCustomInfoValueQry = "SELECT * FROM maintenance_data where form_id='$formId' and `maintenance_form_id` = '$maintenanceId' and form_field_id='$formFieldId'";
                $result1 = mysqli_query($con, $getCustomInfoValueQry);
                while ($rowResult1 = mysqli_fetch_assoc($result1)) {
                    //print_r($rowResult1);
                    $rowResult['value'] = $rowResult1['value'];
                }
                array_push($customInfoList, $rowResult);
            }



            $formDetail['formFields'] = $formFieldList;
            $formDetail['formCustomInfos'] = $customInfoList;
            return $formDetail;
        }
    }
}
//$json = json_encode($CategoryList,JSON_FORCE_OBJECT);
$json = json_encode($ResponseObject);
echo $json;
exit;

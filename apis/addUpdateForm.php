<?php
include('../connection/connection.php');
// Fetch content and determine boundary
$raw_data = file_get_contents('php://input');
//print_r($raw_data);
$mainData = json_decode($raw_data, true);
foreach($mainData as $data){
    //print_r($data);
}


$name = $mainData['name'];
$description = $mainData['description'];
$categoryId = $mainData['category_id'];

$formCustomInfos = $mainData['formCustomInfos'];
$formFields = $mainData['formFields'];
$tag = $mainData['tag'];



if ($tag == 'add') {
    $insertFormQry = "INSERT INTO form (name, description) VALUES ('$name','$description')";
    $resultFormQry = mysqli_query($con, $insertFormQry);
    if (!$resultFormQry) {
        printf("Errormessage: %s\n", mysqli_error($con));
        exit;
    }
    $formId = mysqli_insert_id($con);
    if ($formId) {
        echo 'success';
    } else {
        echo 'error';
    }
} else {
    //update
}

foreach ($formCustomInfos as $key => $value) {
    $description = $value['description'];
    $insertFieldQry = "INSERT INTO form_field (form_id, description, type) VALUES ('$formId','$description','Custom')";
    $resultQry = mysqli_query($con, $insertFieldQry);
    if (!$resultQry) {
        printf("Errormessage: %s\n", mysqli_error($con));
        exit;
    }
}

foreach ($formFields as $key => $value) {
    $description = $value['description'];
    $values = $value['values'];
    $type = $value['type'];
    $positive_value = $value['positive_value'];
    $negative_value = $value['negative_value'];
    $insertFieldQry = "INSERT INTO form_field (form_id, description, `type`,`selection_values`,positive_value,negative_value) VALUES ('$formId','$description','$type','$values','$positive_value','$negative_value')";
    $resultQry = mysqli_query($con, $insertFieldQry);
    if (!$resultQry) {
        printf("Errormessage: %s\n", mysqli_error($con));
        exit;
    }
}

exit;

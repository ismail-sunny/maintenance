<?php
include('../connection/connection.php');
$getCategoryQry = "SELECT * FROM category";

$result = mysqli_query($con,$getCategoryQry);


// Get the data
$CategoryList = array();

if ($result) {
    $count = mysqli_num_rows($result);
    $i = 0;
    while ($row = mysqli_fetch_assoc($result)) {
        //$CategoryList[$i]['id']  = $row['id'];
        //$CategoryList[$i]['name']  = mb_convert_encoding($row['name'], "UTF-8");
        //$CategoryList[$i]['description']  = mb_convert_encoding($row['description'], "UTF-8");
        array_push($CategoryList,$row);
        $i++;
    }
}
//$json = json_encode($CategoryList,JSON_FORCE_OBJECT);
$ResponseObject = new \stdClass;
$ResponseObject->IsSuccess = true;
$ResponseObject->Message = "Successfull";
$ResponseObject->Response = $CategoryList;
$json = json_encode($ResponseObject);
echo $json;
exit;

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Lets mantain everything">
    <meta name="keywords" content="mantenance">
    <title>Login Page | Maintenance</title>

    <!-- Favicons-->
    <link rel="icon" href="images/maintenance.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/maintenance.png.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/maintenance.png.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->

    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/layouts/page-center.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">

</head>

<body class="cyan">
    <!-- Start Page Loading -->
    <div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <!-- End Page Loading -->


    <div id="login-page" class="row" ng-app="MaintenanceApp" ng-controller="panelCtrl">
        <div class="col s12 z-depth-4 card-panel">
            <form class="login-form">
                <div class="row">
                    <div class="input-field col s12 center">
                        <img src="images/maintenance.png" alt="" class="rectangle responsive-img valign profile-image-login">
                        <p class="center login-form-text">Maintenance</p>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <i class="mdi-social-person-outline prefix"></i>
                        <input id="username" type="text" ng-model="formData.email">
                        <label for="username" class="center-align">Email</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12">
                        <i class="mdi-action-lock-outline prefix"></i>
                        <input id="password" type="password" ng-model="formData.password">
                        <label for="password">Password</label>
                    </div>
                </div>
                <div class="row margin">
                    <div class="input-field col s12" ng-show="errorMessage">
                        <i class="mdi-alert-error" style="color:red"></i>
                        <b style="color:red"> {{errorMessage}} </b>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <a class="btn waves-effect waves-light col s12" ng-click="LoginUser()">Login</a>
                    </div>
                </div>
                <!--div class="row">
          <div class="input-field col s6 m6 l6">
            <p class="margin medium-small"><a href="page-register.html">Register Now!</a></p>
          </div>
          <div class="input-field col s6 m6 l6">
              <p class="margin right-align medium-small"><a href="page-forgot-password.html">Forgot password ?</a></p>
          </div>          
        </div-->

            </form>
        </div>
    </div>



    <!-- ================================================
    Scripts
    ================================================ -->

    <!-- jQuery Library -->
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
    <!--materialize js-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--prism-->
    <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
    <!--scrollbar-->
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>

    <!--plugins.js - Some Specific JS codes for Plugin Settings-->
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <!--custom-script.js - Add your own theme custom JS-->
    <script type="text/javascript" src="js/custom-script.js"></script>
    <script type="text/javascript" src="js/angular.min.js"></script>
    <!--angular-materialize js-->
    <script type="text/javascript" src="js/plugins/angular-materialize.js"></script>
    <script>
        $('.datepicker').pickadate({
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 5, // Creates a dropdown of 15 years to control year
            format: 'yyyy-mm-dd'
        });
        var myApp = angular.module('MaintenanceApp', ['ui.materialize']);
        //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
        myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location, $window) {
            $scope.formView = false;
            $scope.floatShow = true;
            $scope.dataListView = true;

            $scope.absUrl = $location.absUrl();
            //console.log($scope.absUrl);
            $scope.EditUserForm = function(value) {
                $scope.formView = true;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.userDetailShow = false;
                $scope.userForm = value;
                $scope.formTag = "Edit User";
            };


            $scope.getUserList = function() {
                $scope.loadingR = true;
                $http({
                    method: 'GET',
                    url: 'apis/GetUserList.php?'
                }).then(function(response) {
                    console.log(response.data, response.status);
                    $scope.userList = response.data.Response;
                    console.log($scope.userList);
                    $scope.loadingR = false;
                }, function(response) {
                    // on error
                    //console.log(response.data, response.status);
                });
            };

            $scope.LoginUser = function() {
                $scope.scheduleFormView = false;
                $scope.scheduleListView = true;
                $scope.floatShow = true;
                //alert("here");
                //alert($scope.formData);
                $http.post('apis/LoginUser.php?', JSON.stringify($scope.formData)).then(function(response) {
                    console.log(response.data);
                    if (response.data.IsSuccess === true) {
                        //$location.path();
                        //alert($location.path());
                        // change the path
                        //$location.path('/UserPanel.php')
                        $scope.errorMessage = response.data.Message;
                        $window.location.href = '/UserPanel.php';
                        //$scope.getFormList();
                    } else {
                        $scope.errorMessage = response.data.Message;
                    }
                }, function(response) {
                    $scope.msg = "Service not Exists";
                    $scope.statusval = response.status;
                    $scope.statustext = response.statusText;
                    $scope.headers = response.headers();
                });

            };
        });
    </script>
</body>

</html>
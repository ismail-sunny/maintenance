<?php
include 'header.php';
?>
<section id="content">
    <div class="container" ng-controller="panelCtrl">

        <div id="table-datatables">
            <div class="col s12 m8 l9 card-panel" ng-show="formDetailShow">
                <h5 class="header">Form Detail</h5>
                <b class="header">{{formDetail.name}}</b><br>
                <b class="header">{{formDetail.description}}</b><br>
                <b class="header">Category: {{formDetail.category}}</b>
                <ul class="collection with-header">
                    <li class="collection-header">
                        <b>Basic Informations</b>
                    </li>
                    <li class="collection-item" ng-repeat="customInfo in formDetail.formCustomInfos">
                        <blockquote><b>{{$index+1}} {{customInfo.description}}</b></blockquote>
                    </li>
                </ul>

                <ul class="collection with-header">
                    <li class="collection-header">
                        <b>Check Lists</b>
                    </li>
                    <li class="collection-item" ng-repeat="formField in formDetail.formFields">
                        <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                        <p>Type : {{formField.type}}
                            <br> Values: {{formField.selection_values}}
                            <br> Positive Value: {{formField.positive_value}}
                            <br> Negative Value: {{formField.negative_value}}
                        </p>
                    </li>
                </ul>
                <div class="row">
                    <div class="input-field" style="right: 10px;">
                        <button class="btn red waves-effect waves-light right" ng-click="CloseFormDetail()">Close
                            <i class="mdi-navigation-cancel right"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-panel" ng-show="formView">
                <nav>
                    <div class="nav-wrapper">
                        <div class="col s12">
                            <a href="#!" class="breadcrumb"><b>Create Maintainance Form</b></a>
                        </div>
                    </div>
                </nav>
                <form class="col s12">
                    <div class="row">
                        <div class="input-field col s12">
                        <h3><b>Create Maintainance Form</b></h3><br>
                            <b>{{formData.name}}<br>
                                {{formData.description}}<br>
                                Category: {{formData.category}}</b>
                        </div>
                    </div>
                    <ul class="collection with-header yellow darken-4">
                        <li class="collection-header">
                            <h6><b>Form Custom Information</b></h6>
                        </li>
                        <li class="collection-item" ng-repeat="formField in formData.formCustomInfos">
                            <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input placeholder="" ng-model="formField.value" type="text">
                                    <label>{{formField.description}}</label>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="collection with-header green accent-4">
                        <li class="collection-header">
                            <b>Check Lists</b>
                        </li>
                        <li class="collection-item" ng-repeat="formField in formData.formFields">
                            <blockquote><b>{{$index+1}}. {{formField.description}}</b></blockquote>
                            <p>Type : {{formField.type}}
                                <br> Values: {{formField.selection_values}}
                                <br> Positive Value: {{formField.positive_value}}
                                <br> Negative Value: {{formField.negative_value}}
                            </p>
                        </li>
                    </ul>


                    <div class="row">
                        <div class="input-field" style="right: 10px;">
                            <button class="btn red waves-effect waves-light right" ng-click="CloseForm()">Cancel
                                <i class="mdi-navigation-cancel right"></i>
                            </button>
                        </div>
                        <div class="input-field" style="right: 20px;">
                            <button class="btn cyan waves-effect waves-light right" ng-click="SubmitMaintanceForm()">Submit
                                <i class="mdi-content-send right"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div ng-show="dataListView">
                <h4 class="header">Form List</h4>
                <!--table class="responsive-table display stripped" cellspacing="0"-->
                <table class="stripped bordered">
                    <thead>
                        <tr class="customth">
                            <th>Name</th>
                            <th>Description</th>
                            <th>Category</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="x in formList">
                            <td>{{ x.name }}</td>
                            <td>{{ x.description }}</td>
                            <td>{{ x.category}}</td>
                            <td>
                                <!--button class="btn cyan waves-effect waves-light" style="right: 10px;" ng-click="CreateMaintenanceSheetForm(x.id)"><i class="mdi-content-send right"></i>New Maintainace Sheet</button>
                                        <button class="btn green waves-effect waves-light" style="right: 10px;" ng-click="GetFormDetail(x.id)"><i class="mdi-content-send right"></i>Detail</button-->
                                <div class="tooltip">
                                    <a class="btn-floating waves-effect waves-light deep-orange" ng-click="CreateMaintenanceSheetForm(x.id)"><i class="mdi-action-assignment-turned-in"></i></a>
                                    <span class="tooltiptext">New Maintainace Sheet</span>
                                </div>
                                <div class="tooltip">
                                    <a class="btn-floating waves-effect waves-light blue-grey" ng-click="GetFormDetail(x.id)"><i class="mdi-action-assignment"></i></a>
                                    <span class="tooltiptext">Detail</span>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- Floating Action Button -->
        <div class="fixed-action-btn" style="bottom: 50px; right: 19px;" ng-show="floatShow">
            <a class="btn-floating btn-large" ng-click="ShowForm()">
                <i class="mdi-content-add"></i>
            </a>
        </div>
        <!-- Floating Action Button -->
    </div>
    <!--end container-->
</section>
<!-- END CONTENT -->
<?php
include 'footer.php';
?>

<script type="text/javascript" src="js/plugins/data-tables/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="js/plugins/data-tables/data-tables-script.js"></script>



<script>
    var myApp = angular.module('MaintenanceApp', ['ui.materialize', 'oitozero.ngSweetAlert']);
    //var myApp = angular.module('ceasbd', ['naif.base64','ui.tinymce','ngSanitize']);
    myApp.controller('panelCtrl', function($scope, $rootScope, $http, $location, $window, SweetAlert) {

        $scope.formView = false;
        $scope.floatShow = true;
        $scope.dataListView = true;
        $scope.absUrl = $location.absUrl();
        //console.log($scope.absUrl);
        $scope.LogOut = function() {
            $http.post('apis/logout.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                $scope.errorMessage = response.data.Message;
                $window.location.href = '/Login.php';
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };
        $scope.formFieldList = [{
            description: '',
            values: '',
            type: '',
            positive_value: '',
            negative_value: ''
        }];

        $scope.formCustomInfoList = [{
            description: '',
            info_type: 'custom'
        }];

        $scope.formFieldListInDb = $scope.formFieldList;

        $scope.ResetForm = function() {
            $scope.formData = {
                remarks: "",
                name: "",
                description: "",
                id: 0
            };
        }

        $scope.ResetForm();

        $scope.ShowForm = function() {
            $scope.formView = true;
            $scope.dataListView = false;
            $scope.floatShow = false;
            $scope.formDetailShow = false;
        };

        $scope.CloseForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
            $scope.ResetForm();
        };

        $scope.CloseFormDetail = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            $scope.formDetailShow = false;
        };

        $scope.getFormList = function() {
            SweetAlert.swal({
                title: "Loading...",
                imageUrl: "images/loading2.gif",
                showCancelButton: false,
                showConfirmButton: false,
                html: true,
            });
            $http({
                method: 'GET',
                url: 'apis/GetFormList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.formList = response.data.Response;
                console.log($scope.formList);
                SweetAlert.swal.close();
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getFormList();

        $scope.getCategory = function() {
            $scope.loadingR = true;
            $http({
                method: 'GET',
                url: 'apis/GetCategoryList.php?'
            }).then(function(response) {
                // on success
                //console.log(response.data, response.status);
                $scope.categoryList = response.data.Response;
                console.log($scope.categoryList);
                $scope.loadingR = false;
            }, function(response) {
                // on error
                //console.log(response.data, response.status);
            });
        };
        $scope.getCategory();

        $scope.GetFormDetail = function(value) {
            var data = {
                form_id: value
            };
            $http.post('apis/GetFormDetail.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data.Response);
                $scope.formDetail = response.data.Response;
                $scope.formView = false;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.formDetailShow = true;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };
        $scope.CreateMaintenanceSheetForm = function(value) {
            var data = {
                form_id: value
            };
            $http.post('apis/GetFormDetail.php?', JSON.stringify(data)).then(function(response) {
                console.log(response.data.Response);
                $scope.formData = response.data.Response;
                $scope.formView = true;
                $scope.dataListView = false;
                $scope.floatShow = false;
                $scope.formDetailShow = false;
            }, function(response) {
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });
        };

        $scope.SubmitMaintanceForm = function() {
            $scope.formView = false;
            $scope.dataListView = true;
            $scope.floatShow = true;
            //alert("here");
            //alert($scope.formData);
            console.log($scope.selectedCategory);
            console.log($scope.formData);
            $scope.formData.tag = "add";

            SweetAlert.swal({
                title: "Submitting...",
                imageUrl: "images/loading2.gif",
                showCancelButton: false,
                showConfirmButton: false,
                html: true,
            });

            $http.post('apis/CreateMaintenanceForm.php?', JSON.stringify($scope.formData)).then(function(response) {
                console.log(response.data);
                if (response.data === 'success') {
                    SweetAlert.swal.close();
                    $scope.CloseFormDetail();
                    //$scope.GetMaintenanceSheetList($scope.formData.maintenance_id, $scope.formData.form_id);
                    SweetAlert.swal("Successfull", "Maintenance Form Created!", "success");
                    //$scope.getFormList();
                } else {
                    SweetAlert.swal("Error", response.data);
                }
            }, function(response) {
                $scope.msg = "Service not Exists";
                SweetAlert.swal("Error", response.data);
                $scope.statusval = response.status;
                $scope.statustext = response.statusText;
                $scope.headers = response.headers();
            });

        };



        $scope.$watch('op', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });
        $scope.$watch('type', function() {
            $scope.loading = true;
            $scope.bundleOfferList = null;
            setTimeout(function() {
                if ($scope.bundle === true) {
                    $scope.getBundleOffer($scope.op, $scope.type);
                }
                $scope.loading = false;
            }, 500);
        });

    });
</script>